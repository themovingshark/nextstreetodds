module(..., package.seeall)
backgroundImage = "images/dashboard-background.png";  
cardChooserBackgroundImage = "images/felt-background.png";
buttonPressSound = "sound/click.mp3"
switchSceneSound = "sound/shake_sound2.mp3"
toggleSound = "sound/click-01.mp3"

soundOnImage = "images/sound-on.png"
soundOnPressedImage = "images/sound-on-pressed.png"
soundOffImage = "images/sound-off.png"
soundOffPressedImage = "images/sound-off-pressed.png"

--********************dial and needle properties
dialImage = "images/dial.png"
dialSizeWidth = 160  --156 -- original size 176
dialSizeHeight = dialSizeWidth -- dialSizeWidth - 50
needleImage = "images/needle.png"
needleSizeWidth = dialSizeWidth
needleSizeHeight = dialSizeHeight

verticalDialSpacing = dialSizeHeight+ 30
horizDialSpacing = dialSizeWidth+ 40-- 65 
dialLabelSpacing = 92   --space between dial and dial label

dialLabelFontSize = 20
dialPercentLabelFontSize = 26

--***********************dial description properties
dialDescTextLine1 = "Showing % Chance Of"
dialDescTextLine1X = 410
dialDescTextLine1Y = 850
dialDescLine1FontSize = 35
dialDescTextLine1Color = { 1, 0, 0 }  --red=1, green =0 , blue=0

dialDescTextLine2 = "Improving On The Flop"
dialDescTextLine2X = dialDescTextLine1X
dialDescTextLine2Y = dialDescTextLine1Y +50
dialDescLine2FontSize = dialDescLine1FontSize
dialDescTextLine2Color = { 1, 0, 0 }  --red=1, green =0 , blue=0
--***********************dial description properties

--***********************street label properties
holeCardText = "Hole Cards"
holeCardTextX = 175
holeCardTextY = 195
holeCardTextFontSize =22
holeCardTextColor = { 0, 0, 1 }  --red=0, green =0 , blue=1

flopCardText = "Flop Cards"
flopCardTextX = 250
flopCardTextY = 382
flopCardTextFontSize =22
flopCardTextColor = { 0, 0, 1 }  --red=0, green =0 , blue=1

turnCardText = "Turn Card"
turnCardTextX = 555
turnCardTextY = 382
turnCardTextFontSize =22
turnCardTextColor = { 0, 0, 1 }  --red=0, green =0 , blue=1
--***********************street label properties

--***************Column 1
--dial one
dialOneX = 125
dialOneY = 510
needleOneX = dialOneX
needleOneY= dialOneY
dialOnePercentTextX= dialOneX
dialOnePercentTextY= dialOneY+30
dialOneLabelX=dialOneX
dialOneLabelY=dialOneY+dialLabelSpacing
dialOneLabelText = "1 PAIR"

--dial two
dialTwoX = dialOneX  --same x as dial 1
dialTwoY = dialOneY+verticalDialSpacing
needleTwoX = dialTwoX
needleTwoY= dialTwoY
dialTwoPercentTextX= dialTwoX
dialTwoPercentTextY= dialTwoY+30
dialTwoLabelX=dialTwoX
dialTwoLabelY=dialTwoY+dialLabelSpacing
dialTwoLabelText = "STRAIGHT"

--dial three
dialThreeX = dialOneX-- (dialOneX + dialOneX  + horizDialSpacing)/2  --halfway between dial 1 and 4
dialThreeY = dialTwoY+verticalDialSpacing
needleThreeX = dialThreeX
needleThreeY= dialThreeY
dialThreePercentTextX= dialThreeX
dialThreePercentTextY= dialThreeY+30
dialThreeLabelX=dialThreeX
dialThreeLabelY=dialThreeY+dialLabelSpacing
dialThreeLabelText = "4 of KIND"  

--***************Column 2
--dial four
dialFourX = dialOneX + horizDialSpacing
dialFourY = dialOneY --same as dial 1 y
needleFourX = dialFourX
needleFourY= dialFourY
dialFourPercentTextX= dialFourX
dialFourPercentTextY= dialFourY+30
dialFourLabelX=dialFourX
dialFourLabelY=dialFourY+dialLabelSpacing
dialFourLabelText = "2 PAIR"

--dial five
dialFiveX = dialFourX  --same x as dial 4
dialFiveY = dialFourY+verticalDialSpacing
needleFiveX = dialFiveX
needleFiveY= dialFiveY
dialFivePercentTextX= dialFiveX
dialFivePercentTextY= dialFiveY+30
dialFiveLabelX=dialFiveX
dialFiveLabelY=dialFiveY+dialLabelSpacing
dialFiveLabelText = "FLUSH"

--dial six
dialSixX = (dialFourX + dialFourX + horizDialSpacing)/2  --halfway between dial 4 and 7 
dialSixY = dialFiveY+verticalDialSpacing
needleSixX = dialSixX
needleSixY= dialSixY
dialSixPercentTextX= dialSixX
dialSixPercentTextY= dialSixY+30
dialSixLabelX=dialSixX
dialSixLabelY=dialSixY+dialLabelSpacing
dialSixLabelText = "REMOVE OLD 4 of KIND"

--***************Column 3
--dial seven
dialSevenX = dialFourX + horizDialSpacing
dialSevenY = dialOneY --same as dial 1 y
needleSevenX = dialSevenX
needleSevenY= dialSevenY
dialSevenPercentTextX= dialSevenX
dialSevenPercentTextY= dialSevenY+30
dialSevenLabelX=dialSevenX
dialSevenLabelY=dialSevenY+dialLabelSpacing
dialSevenLabelText = "3 of KIND"

--dial Eight
dialEightX = dialSevenX  --same x as dial 7
dialEightY = dialSevenY+verticalDialSpacing
needleEightX = dialEightX
needleEightY= dialEightY
dialEightPercentTextX= dialEightX
dialEightPercentTextY= dialEightY+30
dialEightLabelX=dialEightX
dialEightLabelY=dialEightY+dialLabelSpacing
dialEightLabelText = "FULL HOUSE"

	--[[random hand dial
	local againstRandomDialx = (_W*.5) - 180
	local againstRandomDialy = dialYposition
	local randomHandNeedlex = againstRandomDialx
	local randomHandNeedley = againstRandomDialy
	local randomHandPercentTextx = againstRandomDialx
	local randomHandPercentTexty = againstRandomDialy + 30
	local randomHandDialLabelx = againstRandomDialx
	local randomHandDialLabely = againstRandomDialy +130
	--]]

selectedSquare = "images/cardbuttons/selectedsquare-yellow.png"
	 
cardBackImage = "images/card-back.png"  --size is 159x220
cardBackImageWidthSize = 159
cardBackImageHeightSize = 220
holeCardScaleFactor = .71
flopCardScaleFactor = .71 --.90
turnCardScaleFactor = flopCardScaleFactor
--*********card size
holeCardWidth =cardBackImageWidthSize * holeCardScaleFactor
holeCardHeight = cardBackImageHeightSize * holeCardScaleFactor
flopCardWidth =cardBackImageWidthSize * flopCardScaleFactor
flopCardHeight = cardBackImageHeightSize * flopCardScaleFactor
turnCardWidth =cardBackImageWidthSize * turnCardScaleFactor
turnCardHeight = cardBackImageHeightSize * turnCardScaleFactor
--*********card size

--*********card location
--****HOLE
cardSpacing = holeCardWidth+ 30 --distance between card x-coords
card1Imagex = 40
card1Imagey = 100  --135
card2Imagex = card1Imagex + cardSpacing
card2Imagey = card1Imagey
--****FLOP
flopCardSpacing = flopCardWidth + 40 --distance between card x-coords
flopCard1Imagex = card1Imagex 
flopCard1Imagey = 290  --135
flopCard2Imagex = flopCard1Imagex + flopCardSpacing
flopCard2Imagey = flopCard1Imagey
flopCard3Imagex = flopCard2Imagex + flopCardSpacing
flopCard3Imagey = flopCard1Imagey
--****TURN
turnCardImagex = flopCard3Imagex + flopCardSpacing
turnCardImagey = flopCard1Imagey
--*********card location	

--**Button locations
newHandButtonx = 400
newHandButtony = 110
newHandButtonSize = 110

--******Scene stuff
--[[   Possible transition effect strings
	fade
zoomOutIn
zoomOutInFade
zoomInOut
zoomInOutFade
flip
flipFadeOutIn
zoomOutInRotate
zoomOutInFadeRotate
zoomInOutRotate
zoomInOutFadeRotate
fromRight (over original scene)
fromLeft (over original scene)
fromTop (over original scene)
fromBottom (over original scene)
slideLeft (pushes original scene)
slideRight (pushes original scene)
slideDown (pushes original scene)
slideUp (pushes original scene)
crossFade
	--]]
startupSceneTransitionType = "fade"
startupSceneTransitionTime = 400
--sceneTransitionType = "flipFadeOutIn"
sceneTransitionType = "zoomInOutFade"
sceneTransitionTime = 400

mainScreenHelpButtonX = 550
mainScreenHelpButtonY = 110
mainScreenHelpButtonSize = newHandButtonSize

soundButtonX = 550
soundButtonY = 110

_G.isSoundOn = true  --global variable to keep track of whether the sound is on or not.  used to keep the sound button from getting reset on new hand

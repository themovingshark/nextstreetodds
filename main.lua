-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------
local uiGlobals = require("ui_globals") 
local composer = require "composer"

-- load mainscreen.lua
local options = {
   effect = uiGlobals.startupSceneTransitionType, 
   time = uiGlobals.startupSceneTransitionTime,
   params = {
		hand="",
		card1="",  --hole card 1
		card1Value = "",  --value of card such as 2 or j
		card2="",  --hole card 1
		card2Value = "", 
		chenFormula=0,
		numcardstochoose= 0,
		--soundOn=true,
		soundOn = _G.isSoundOn,
		gameType= 0 , --loose game
		flopCard1 ="",
		flopCard1Value = "",
		flopCard2="",
		flopCard2Value = "",
		flopCard3="",
		flopCard3Value = "",
		turnCard="",
		turnCardValue = "",
		choosingWhichCards = 1,   --1= hole, 2= flop, 3= turn
		completeHand = "",
		selectedHoleCards = {},
		selectedFlopCards = {},
		selectedTurnCard = {},
		alreadySelectedCards = {},
		holePocketPair = false,
		holeSuited = false,
		fourFlush = false,
		flushAchieved = false,
		openEndedStraightDraw = false,
		gutshotStraightDraw = false,
		areConnected = false,
		twoToStraight = false,
		threeToStraight = false,
		fourToStraight = false
	}
}
composer.gotoScene( "mainscreen" ,options)
--composer.gotoScene( "cardchooserscreen" ,options)

-- Add any objects that should appear on all scenes below (e.g. tab bar, hud, etc.):

module(..., package.seeall)
local constantsGlobals = require ("global_constants")

--set up the string for the different decisions (cases)
pocketPairs = "AAOKKOQQOJJOTTO99O88O77O66O55O44O33O22O"
highPocketPairs = "AAOKKOQQOJJOTTO"   --10s and above
highSuited = "AKSAQSAJSATS"  --AKs through ATs
highUnsuited = "AKOAQOAJOATO"  --AKo through ATo
suitedAces = "AKSAQSAJSATSA9SA8SA7SA6SA5SA4SA3SA2S"  --suited aces


--suitedKings = 
Case1 = highPocketPairs .. "99O" .. "AKSAQSAJSATS" .. "KQSKJS" .. "AKSAKOAQSAQOAJSAJO" .. "KQSKQO"
Case2 = pocketPairs .. "A9SA8SA7SA6SA5SA4SA3SA2S" .. "KTSK9S" .. "QJSQTSQ9S" .. "JTSJ9S" .. "T9S98S" .. "ATO" .. "KJO"
Case3 = "AAOKKOQQO" .. "AKS"
Case4 =  "JJOTTO" .. "AQSAJS" .. "KQS" .. "AKO"
Case5 = "AAOKKOQQOJJOTTO99O" .. "AKSAQS" .. "AKOAQO"
Case6 = pocketPairs .. "AJSATS" .. "KQSKJSKTS" .. "QJS" .. "JTS" 
Case7 = highPocketPairs .. "99O88O" .. highSuited .. "A9SA8S" .. "K9S" .. "AKSAQSAJSAKOAQOAJO" .. "KQSKQO"
Case8 = Case1 ..Case2 .. "K8SK7SK6SK5SK4SK3SK2S" .. "Q8S" .. "J8S" .. "87S76S65S54S43S" .. "T8S97S86S75S64S53S"
Case9 = Case3   --same hands as case3 
Case10 = Case4   --same hands as case 4
Case11 = highPocketPairs .. "AKSAQSAJS" .. "KQS" .. "AKO"
Case12 = pocketPairs .. highUnsuited .. suitedAces
Case13 = "AAOKKOQQOJJOTTO99O" .. highSuited .. "KQSKJS" .. "AKOAQO"
Case15 = highPocketPairs .. "AKSAQSAJS" .. "KQS" .. "AKO"
Case16 = Case6
Case18 = highPocketPairs .. "AKSAQSAJS" .. "AKOAQO"
Case19 = "99O88O77O" .. "ATSKQSKJSKTSAJOKQOQJSQTSJTS"
Case20 = highPocketPairs .. "AKSAKO"
Case21 = "AQSAJSKQS"
Case22 = "AAOKKOQQOAKS"
Case23 = highPocketPairs .. "99O" .. "AKSAQSAJSATS" .. "KQSKJS" .. "AKOAQOAJO" .. "KQO"
Case24 = "88O77O66O55O44O33O22O" .. suitedAces .. "KTSKQSKJSKTSK9S" .. "QJSQTSQ9S" .. "JTSJ9S" .. "T9S98S" .."ATOKJO"
Case25 = "AAOKKOQQO" .. "AKS"
Case26 = highPocketPairs .. "AKSAKO"
Case27 = "AQSAJSKQS"
Case28 = highPocketPairs .. "99O" .. "AKSAQSAJSATSA9SA8S" .. "KQSKJSKTS" .. "QJS" .. "AKOAQOAJOATO" .. "KQO"
Case29 = Case23 .. Case24 .. "87S76S65S54S" .. highUnsuited
Case30 = "QJSJTST9S" .. pocketPairs
Case31 = highPocketPairs .. "AKSAKO"
Case32 = "AQSAJSKQS"
Case33 = "AAOKKOQQO" .. "AKS"
Case34 = highPocketPairs .. "99O" .. "AKSAQSAJSATS" .. "KQSKJS" .. "AKOAQO"
Case35 = Case28 .. Case29  --also add suited cards in test
Case36 = Case20 .. Case21 .. pocketPairs
Case37 = Case34
Case38 = highPocketPairs .. "AKSAKO"
Case39 = pocketPairs .. suitedAces .. "KQSKJSKTSK9S" .. "QJSQTSQ9S" .. "JTSJ9S" .. "T9S98S" .. "AKOAQOAJO" .. "87S76S65S54S" .. "KQO"


--[[  position can be:	POSITIONearly(), POSITIONmiddle(), POSITIONlate(),POSITIONSsb(), or POSITIONbb()
--]]
function MoveDeciderForLooseGames(position,startingHand, facingRaise, facingReRaise )
	print("*****IN MoveDeciderForLooseGames startingHand = " .. startingHand)
	print(facingRaise)
	print( facingReRaise)
	local move
	local areSuited = false  --use to check for suitedness
	if (string.find(startingHand, "S") ~= nil) then  --cards are not suited
		areSuited = true
	end
	print("cards are suited")
	
	--the case values will have a number (whether the hand is in the string/true) or it will be nil (not found / false)
	--set the case values, then check them
	local Case1Value = string.find(Case1, startingHand)
	local Case2Value = string.find(Case2, startingHand)
	local Case3Value = string.find(Case3, startingHand)
	local Case4Value = string.find(Case4, startingHand)
	local Case5Value = string.find(Case5, startingHand)
	local Case6Value = string.find(Case6, startingHand)
	local Case7Value = string.find(Case7, startingHand)
	local Case8Value = string.find(Case8, startingHand)
	local Case9Value = string.find(Case9, startingHand)
	local Case10Value = string.find(Case10, startingHand)
	local Case11Value = string.find(Case11, startingHand)
	local Case12Value = string.find(Case12, startingHand)
	local Case13Value = string.find(Case13, startingHand)
   --case 14 is case8value or suited cards
	local Case15Value = string.find(Case15, startingHand)
	local Case16Value = string.find(Case16, startingHand)
	
	if (position == constantsGlobals.POSITIONearly)  or (position == constantsGlobals.POSITIONmiddle) then  --if early or middle position
	--[[ *************************************************************************************************
			                                              EARLY AND MIDDLE POSITION TESTS
		*************************************************************************************************--]]
		print("**position is early or middle")
		if (facingRaise == false) then --not facing RAISE
			print("not facing raise")
			--print (Case1Value, Case1)
			if (Case1Value ~=nil) then   --case 1 is TRUE
				print("case 1 true")
				move = constantsGlobals.RAISE
			else --case 1 TRUE, check case 2
				if (Case2Value ~= nil) then  -- case 2 is TRUE
					print("case 2 true")
					move = constantsGlobals.CALL
				else  -- case 2 is FALSE
					print("case 2 false")
					move = constantsGlobals.FOLD
				end  -- end if
			end
		else   --facing RAISE
			print("facing raise")
			if (facingReRaise == false) then  --not facing reraise
				print("not facing reraise")
				if (Case5Value ~= nil) then  --case 5 is TRUE
					print("case 5 true")
					move = constantsGlobals.RERAISE
				else -- case 5 is FALSE
					print("case 5 false")
					if (Case6Value ~= nil) then  --case 6 is TRUE
						print("case 6 true")
						move = constantsGlobals.CALL
					else   -- case 6 is FALSE
						print("case 6 false")
						move = constantsGlobals.FOLD
					end
				end
			else        -- facing reraise
				print("facing reraise")
				if (Case3Value ~= nil) then  --case 3 is TRUE
					print("case 3 false")
					move = constantsGlobals.RAISE
				else   -- case 3 is FALSE
					print("case 3 false")
					if (Case4Value ~= nil) then   --case 4 is TRUE
						print("case 4 true")
						move = constantsGlobals.CALL
					else    --case 4 is FALSE
						print("case 4 false")
						move = fold
					end
				end
			end  --end if facing reraise
		end  --end if facing raise
	elseif (position == constantsGlobals.POSITIONlate) then    --late position check
	--[[ *************************************************************************************************
			                                              LATE POSITION TESTS
		*************************************************************************************************--]]
		print("**position is late")
		if (facingRaise  == false) then  -- not facing raise in late pos
			if (Case7Value ~= nil) then  --case 7 is TRUE
				print("case 7 true")
				move = constantsGlobals.RAISE
			else
				print("case 7 false")
				if (Case8Value ~= nil) then  --case 8 is TRUE
					print("case 8 true")
					move = constantsGlobals.CALL
				else   --case 8 is FALSE
					print("case 8 false")
					move = constantsGlobals.FOLD
				end --testing case 8
			end  --testing case 7
		else   -- facing raise in late pos - check for re-raise
			if (facingReRaise == false) then  -- not facing re-raise in late pos
				if (Case11Value ~= nil) then -- case 11 is TRUE ->raise but no reraise
					print("case 11 true")
					move = constantsGlobals.RERAISE
				else  -- case 11 is FALSE test case 12
					print("case 11 false")
					if (Case12Value ~= nil) then  --case 12 TRUE
						print("case 12 true")
						move = constantsGlobals.CALL
					else
						print("case 12 false")    --case 12 FALSE
						move = constantsGlobals.FOLD
					end   --testing case 12
				end   --testing case 11
			else      --facing re-raise in late pos
			end   -- end facing raise in late pos
		end
	elseif (position == constantsGlobals.POSITIONsb) then    -- small blind position
	--[[ *************************************************************************************************
			                                              SMALL BLIND POSITION TESTS
		*************************************************************************************************--]]
		print("**position is small blind")
		if (facingRaise  == false) then  -- not facing raise in small blind
			print("small blind no raise")
			if (Case13Value ~= nil) then  -- case 13 TRUE
				print("case 13 true")
				move = constantsGlobals.RAISE
			else   -- case 13 FALSE test case 14
				print("case 13 false")
				if ( (Case8Value ~= nil) or (areSuited == true)) then   -- case 14 true
					print("case 14 true")
					move = constantsGlobals.CALL
				else     --case 14 false
					print("case 14 false")
					move = constantsGlobals.FOLD
				end     --testing case 14
			end    --  testing case 13
		else   -- facing raise in small blind
			print("small blind against raise")
			if (Case15Value ~= nil) then   -- case 15 TRUE
				print("case 15 true")
				move = constantsGlobals.RERAISE
			else     -- case 15  FALSE test case 16
				print("case 15 false")
				if (Case16Value ~= nil) then   --case 16 TRUE
					print("case 16 true")
					move = constantsGlobals.CALL
				else       --case 16 FALSE
					print("case 16 false")
					move = constantsGlobals.FOLD
				end
			end    --testing case 16
		end
	else     --position must be big blind
	--[[ *************************************************************************************************
			                                              BIG BLIND POSITION TESTS
		*************************************************************************************************--]]
		print("**position is big blind")
		if (facingRaise  == false) then  -- not facing raise in big blind
			print("big blind no raise")
			if (Case13Value ~= nil) then  -- case 13 TRUE
				print("case 13 true")
				move = constantsGlobals.RAISE
			else   
				move = constantsGlobals.CHECK
			end    --  testing case 13
		else   -- facing raise in big blind
			print("big blind against raise")
			if (Case11Value ~= nil) then   -- case 11 TRUE
				print("case 11 true")
				move = constantsGlobals.RERAISE
			else     -- testing case 17
				print("case 11 false")
				if (Case17Value ~= nil) then   --case 17 TRUE
					print("case 17 true")
					move = constantsGlobals.CALL
				else       --case 17 FALSE
					print("case 17 false")
					move = constantsGlobals.FOLD
				end
			end    --testing case 17
		end
	end  --end if test of position ==
	
	return constantsGlobals.moveText[move]  --return text for move
end  -- end function

function MoveDeciderForTightGames(position,startingHand, facingRaise, facingReRaise )
	print("*****IN MoveDeciderForTightGames startingHand = " .. startingHand)
	print(facingRaise)
	print( facingReRaise)
	local move
	local areSuited = false  --use to check for suitedness
	if (string.find(startingHand, "S") ~= nil) then  --cards are not suited
		areSuited = true
	end
	print("cards are suited")
	
	--the case values will have a number (whether the hand is in the string/true) or it will be nil (not found / false)
	--set the case values, then check them
	local Case18Value = string.find(Case18, startingHand)
	local Case19Value = string.find(Case19, startingHand)
	local Case20Value = string.find(Case20, startingHand)
	local Case21Value = string.find(Case21, startingHand)
	local Case22Value = string.find(Case22, startingHand)
	local Case23Value = string.find(Case23, startingHand)
	local Case24Value = string.find(Case24, startingHand)
	local Case25Value = string.find(Case25, startingHand)
	local Case26Value = string.find(Case26, startingHand)
	local Case27Value = string.find(Case27, startingHand)
	local Case28Value = string.find(Case28, startingHand)
	local Case29Value = string.find(Case29, startingHand)
	local Case30Value = string.find(Case30, startingHand)
	local Case31Value = string.find(Case31, startingHand)
	local Case32Value = string.find(Case32, startingHand)
	local Case33Value = string.find(Case33, startingHand)
	local Case34Value = string.find(Case34, startingHand)
	local Case35Value = string.find(Case35, startingHand)
	local Case36Value = string.find(Case36, startingHand)
	local Case37Value = string.find(Case37, startingHand)
	local Case38Value = string.find(Case38, startingHand)
	local Case39Value = string.find(Case39, startingHand)
	
	if (position == constantsGlobals.POSITIONearly) then  --if early or middle position
	--[[ *************************************************************************************************
			                                              EARLY POSITION TESTS
		*************************************************************************************************--]]
		print("**position is early")
		if (facingRaise == false) then --not facing RAISE
			print("not facing raise")
			if (Case18Value ~= nil) then  -- case 18 TRUE
				move = constantsGlobals.RAISE
				else --test case 19
					if (Case19Value ~= nil) then  --case 19 TRUE
						move = constantsGlobals.CALL
					else
						move = constantsGlobals.FOLD
					end   --case 19 test
			end --case 18 test
		else   --facing RAISE
			print("facing raise")
			if (facingReRaise == false) then   --no facing reraise
				print("not facing reraise")
				if (Case20Value ~= nil) then   -- case 20 TRUE
					print("case 20 true")
					move = constantsGlobals.RERAISE
				else
					print("case 20 false")
					if (Case21Value ~= nil) then   --case 21 TRUE
						print("case 21 true")
						move = constantsGlobals.CALL
					else     --case 21 FALSE
						print("case 21 false")
						move = constantsGlobals.FOLD
					end
				end   --case 20 test
			else   -- facing reraise
				print("facing reraise")
				if (Case22Value ~= nil) then  --case 22 TRUE
					print("case 22 true")
					move = constantsGlobals.RAISE
				else
					print ("case 22 false")
					move = constantsGlobals.FOLD
				end   -- case 22 test
			end   --facing reraise test
		end  --end if facing raise early
	elseif (position == constantsGlobals.POSITIONmiddle) then    --middle position check
	--[[ *************************************************************************************************
			                                              MIDDLE POSITION TESTS
		*************************************************************************************************--]]
		print("**position is middle")
		if (facingRaise == false) then --not facing RAISE
			print("not facing raise")
			if (Case23Value ~= nil)  then --case 23 is true
				print("case 23 true")
				move = constantsGlobals.RAISE
			else    --case 23 false
				print("case 23 false")
				if (Case24Value ~= nil) then  --case 24 true
					print("case 24 true")
					move = constantsGlobals.CALL
				else     --case 24 false
					print("case 24 false")
					move = constantsGlobals.FOLD
				end   --end case 24 test
			end  --end case 23 test
		else   --facing RAISE
			print("facing raise")
			if (facingReRaise == false)  then   --not facing reraise
				print("not facing reraise")
				if (Case26Value ~= nil) then  --case 26 true
					print("case 26 true")
					move = constantsGlobals.RERAISE
				else     -- case 26 false
					print("case 26 false")
					if (Case27Value ~=nil) then   --case 27 true
						print("case 27 true")
						move = constantsGlobals.CALL
					else      --case 27  false
					print("case 27 false")
						move = constantsGlobals.FOLD
					end    --end case 27 test
				end   --end case 26 test
			else     --facing reraise
				print("facing reraise")
				if (Case25Value ~= nil)  then   --case 25 true
					print("case 25 true")
					move = constantsGlobals.RAISE
				else     --case 25 false
					print("case 25 false")
					move = constantsGlobals.FOLD
				end    --end case 25 test
			end  --testing face reraise
		end  --end if facing raise middle
	elseif (position == constantsGlobals.POSITIONlate) then    --late position check
	--[[ *************************************************************************************************
			                                              LATE POSITION TESTS
		*************************************************************************************************--]]
		print("**position is late")
		if (facingRaise == false) then --not facing RAISE
			print("not facing raise")
			if (Case28Value ~= nil)  then    --case 28 true
				print("case 28 true")
				move = constantsGlobals.RAISE
			else    --case 28 false
				print("case 28 false")
				if (Case29Value ~= nil) then --case 29 true
					print("case 29 true")
					move = constantsGlobals.CALL
				else    --case 29 false
					print("case 29 false")
					move = constantsGlobals.FOLD
				end   --end case 29 test
			end
		else   --facing RAISE
			print("facing raise")
			if (facingReRaise ==false) then   --not facing reraise
				print("not facing reraise")
				if (Case30Value ~= nil) then  --case 30 true
					print("case 30 true")
					move = constantsGlobals.CALL
				else    --case 30 false
					print("case 30 false")
					if (Case31Value ~= nil) then  --case 31 true
						print("case 31 true")
						move = constantsGlobals.RERAISE
					else     --case 31 false
						print("case 31 false")
						if (Case32Value ~= nil) then   --case 32 true
							print("case 32 true")
							move = constantsGlobals.CALL
						else     --case 32 false
							print("case 32 false")
							move = constantsGlobals.FOLD
						end    --end case 32 test
					end   --end case 31 test
				end   --end case 30 test
			else    --facing reraise
				print("facing reraise")
				if (Case33Value ~= nil)  then  -- case 33 true
					print("case 33 true")
					move = constantsGlobals.RAISE
				else   --case 33 false
					print("case 33 false")
					move = constantsGlobals.FOLD
				end   --end case 33 test
			end  --end if facing reraise
		end  --end if facing raise late
	elseif (position == constantsGlobals.POSITIONsb) then    -- small blind position
	--[[ *************************************************************************************************
			                                              SMALL BLIND POSITION TESTS
		*************************************************************************************************--]]
		print("**position is small blind")
		if (facingRaise == false) then --not facing RAISE
			print("not facing raise")
			if (Case34Value ~= nil) then  --case 34 true
				print("case 34 true")
				move = constantsGlobals.RAISE
			else    --case 34 true
				print("case 34 false")
				if (Case35Value ~= nil) then  --case 35 true
					print("case 35 true")
					move = constantsGlobals.CALL
				else   --case 35 false
					print("case 35 false")
					move = constantsGlobals.FOLD
				end    --end case 35 test
			end  --end case 34 test
		else   --facing RAISE
			print("facing raise")
			if (Case36Value ~= nil) then  --case 36 true
				print("case 36 true")
				move = constantsGlobals.CALL
			else    --case 36 false
				print("case 36 false")
				move = constantsGlobals.FOLD
			end    --end case 36 test
		end  --end if facing raise small blind
	else     --position must be big blind
	--[[ *************************************************************************************************
			                                              BIG BLIND POSITION TESTS
		*************************************************************************************************--]]
		 print("**position is big blind")
		if (facingRaise == false) then --not facing RAISE
			print("not facing raise")
			if (Case37Value ~= nil) then   --case 37 true
				print("case 37 true")
				move = constantsGlobals.RAISE
			else      --case 37 false
				print("case 37 false")
				move = constantsGlobals.CHECK
			end
		else   --facing RAISE
			print("facing raise")
			if (Case38Value ~= nil) then    --case 38 true
				print("case 38 true")
				move = constantsGlobals.RERAISE
			else   --case 38 false
				print("case 38 false")
				if (Case39Value ~= nil) then   --case 39 true
					print("case 39 true")
					move = constantsGlobals.CALL
				else    --case 39 false
					print("case 39 false")
					move = constantsGlobals.FOLD
				end     --end case 39 test
			end    --end case 38 test
		end  --end if facing raise big blind
	end  --end if test of position ==
	
	return constantsGlobals.moveText[move]  --return text for move
end  -- end function

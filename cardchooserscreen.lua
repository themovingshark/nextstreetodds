----------------------------------------------------------------------------------
--
-- scenetemplate.lua
--
----------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

local ht = require("handtables");
local ot = require("odds")
local helpText = require("ultimateoddshelp") 
local uiGlobals = require("ui_globals") 
local widget = require "widget"
local params
local cardList = {}
--local previousSelectedCards = params.alreadySelectedCards    --hold current selected cards in case user cancels screen
local previousSelectedHoleCards, previousSelectedFlopCards, previousSelectedTurnCard
---------------------------------------------------------------------------------
--print("in cardchooserscreen")

----------------------------------------------------------------------------------
--
-- shallowCopy - used to copy the contents of 1 table into another
--               usage:  newTable = shallowCopy(originalTable)
--
----------------------------------------------------------------------------------
function shallowCopy(original)
    local copy = {}
    for key, value in pairs(original) do
        copy[key] = value
    end
    return copy
end

function scene:create( event )
    print("IN cardchooserscreen");
	_W = display.contentWidth;
	_H = display.contentHeight;
	local sceneGroup = self.view
	params = event.params
	--previousSelectedHoleCards = params.selectedHoleCards    --hold current selected cards in case user cancels screen
	--previousSelectedFlopCards = params.selectedFlopCards
	--previousSelectedTurnCard = params.selectedTurnCard
	previousSelectedHoleCards = shallowCopy(params.selectedHoleCards)    --hold current selected cards in case user cancels screen
	previousSelectedFlopCards = shallowCopy(params.selectedFlopCards)
	previousSelectedTurnCard = shallowCopy(params.selectedTurnCard)
	
	print("in cardchooserscreen params.hand="..params.hand);
	print("parms.choosingWhichCards = " ..params.choosingWhichCards)
	print("#previousSelectedHoleCards=".. #previousSelectedHoleCards)
	print("---previously selected hole cards---")
	for k, v in pairs( previousSelectedHoleCards ) do
   		print(k, v)
	end
	print("---previously selected hole cards---")
	-- Called when the scene's view does not exist.
	-- 
	-- INSERT code here to initialize the scene
	-- e.g. add display objects to 'sceneGroup', add touch listeners, etc.
	local backgroundImage =uiGlobals.cardChooserBackgroundImage
	--local backgroundImage = "images/metal-background.png";
	local backgroundImg = display.newImageRect( backgroundImage, 640, 1024 )
	backgroundImg.x = display.contentCenterX
	backgroundImg.y = display.contentCenterY
	sceneGroup:insert( backgroundImg)
	
	local switchSceneSound = audio.loadSound ( uiGlobals.switchSceneSound);
    local buttonClickSound = audio.loadSound ( uiGlobals.buttonPressSound);
    

	local xOffsetToCenterCards = 20  --offset the x coord to move cards to the right a little
	local selectedList={}
	local litList={}   --this is the list of display objects containing the yellow squares
	local directionsFontSize = 40;
	local directionsText1 = "Choose"
	local directionsText1X = _W * .5
	local directionsText1Y = _H*.42
	local directionsText2 = "Cards"
	local directionsText2X = _W * .5
	local directionsText2Y = _H*.46

	local helpCardChooserx = 294  + xOffsetToCenterCards
	local helpCardChoosery = 960


	--numeric rankings of card values
	local cardRank = {}
	cardRank["2"] = 2
	cardRank["3"] = 3
	cardRank["4"] = 4
	cardRank["5"] = 5
	cardRank["6"] = 6
	cardRank["7"] = 7
	cardRank["8"] = 8
	cardRank["9"] = 9
	cardRank["t"] = 10
	cardRank["j"] = 11
	cardRank["q"] = 12
	cardRank["k"] = 13
	cardRank["a"] = 14
	
	display.setStatusBar( display.HiddenStatusBar )
--[[
local dispObj_3 = display.newImageRect( "images/cardbuttons/felt-background.png", 600, 1024 )
dispObj_3.x = 300
dispObj_3.y = 512

--]]
local litkc = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
litkc.x = 530 + xOffsetToCenterCards
litkc.y = 822
litkc.isVisible = false


local littc = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
littc.x = 530+ xOffsetToCenterCards
littc.y = 730
littc.isVisible = false


local lit7c = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit7c.x = 530 + xOffsetToCenterCards
lit7c.y = 638
lit7c.isVisible = false	


local litac = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
litac.x = 530 + xOffsetToCenterCards
litac.y = 914
litac.isVisible = false


local litqc = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
litqc.x = 438 + xOffsetToCenterCards
litqc.y = 822
litqc.isVisible = false


local lit9c = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit9c.x = 438 + xOffsetToCenterCards
lit9c.y = 730
lit9c.isVisible = false


local lit6c = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit6c.x = 438 + xOffsetToCenterCards
lit6c.y = 638
lit6c.isVisible = false	


local litjc = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
litjc.x = 346 + xOffsetToCenterCards
litjc.y = 822
litjc.isVisible = false


local lit8c = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit8c.x = 346 + xOffsetToCenterCards
lit8c.y = 730
lit8c.isVisible = false


local lit5c = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit5c.x = 346 + xOffsetToCenterCards
lit5c.y = 638
lit5c.isVisible = false


local litkd = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
litkd.x = 254 + xOffsetToCenterCards
litkd.y = 822
litkd.isVisible = false	


local littd = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
littd.x = 254 + xOffsetToCenterCards
littd.y = 730
littd.isVisible = false	


local lit7d = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit7d.x = 254 + xOffsetToCenterCards
lit7d.y = 638
lit7d.isVisible = false


local litad = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
litad.x = 70 + xOffsetToCenterCards
litad.y = 915
litad.isVisible = false


local litqd = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
litqd.x = 162 + xOffsetToCenterCards
litqd.y = 822
litqd.isVisible = false


local lit9d = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit9d.x = 162 + xOffsetToCenterCards
lit9d.y = 730
lit9d.isVisible = false


local lit6d = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit6d.x = 162 + xOffsetToCenterCards
lit6d.y = 638
lit6d.isVisible = false


local litjd = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
litjd.x = 70 + xOffsetToCenterCards
litjd.y = 822
litjd.isVisible = false


local lit8d = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit8d.x = 70 + xOffsetToCenterCards
lit8d.y = 730
lit8d.isVisible = false


local lit4c = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit4c.x = 530 + xOffsetToCenterCards
lit4c.y = 546
lit4c.isVisible = false


local lit5d = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit5d.x = 70 + xOffsetToCenterCards
lit5d.y = 638
lit5d.isVisible = false


local lit3c = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit3c.x = 438 + xOffsetToCenterCards
lit3c.y = 546
lit3c.isVisible = false


local lit2c = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit2c.x = 346 + xOffsetToCenterCards
lit2c.y = 546
lit2c.isVisible = false


local lit4d = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit4d.x = 254 + xOffsetToCenterCards
lit4d.y = 546
lit4d.isVisible = false


local lit3d = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit3d.x = 162 + xOffsetToCenterCards
lit3d.y = 546
lit3d.isVisible = false


local litas = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
litas.x = 70 + xOffsetToCenterCards
litas.y = 454
litas.isVisible = false


local litah = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
litah.x = 530 + xOffsetToCenterCards
litah.y = 454
litah.isVisible = false


local litkh = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
litkh.x = 530 + xOffsetToCenterCards
litkh.y = 362
litkh.isVisible = false


local litqh = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
litqh.x = 438 + xOffsetToCenterCards
litqh.y = 362
litqh.isVisible = false


local litjh = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
litjh.x = 346 + xOffsetToCenterCards
litjh.y = 362
litjh.isVisible = false


local litqs = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
litqs.x = 162 + xOffsetToCenterCards
litqs.y = 362
litqs.isVisible = false


local litks = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
litks.x = 254 + xOffsetToCenterCards
litks.y = 362
litks.isVisible = false


local litjs = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
litjs.x = 70 + xOffsetToCenterCards
litjs.y = 362
litjs.isVisible = false


local litts = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
litts.x = 254 + xOffsetToCenterCards
litts.y = 270
litts.isVisible = false


local lit8h = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit8h.x = 346 + xOffsetToCenterCards
lit8h.y = 270
lit8h.isVisible = false


local lit9h = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit9h.x = 438 + xOffsetToCenterCards
lit9h.y = 270
lit9h.isVisible = false


local litth = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
litth.x = 530 + xOffsetToCenterCards
litth.y = 270
litth.isVisible = false


local lit9s = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit9s.x = 162 + xOffsetToCenterCards
lit9s.y = 270
lit9s.isVisible = false


local lit2d = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit2d.x = 70 + xOffsetToCenterCards
lit2d.y = 546
lit2d.isVisible = false


local lit8s = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit8s.x = 71 + xOffsetToCenterCards
lit8s.y = 270
lit8s.isVisible = false


local lit7h = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit7h.x = 530 + xOffsetToCenterCards
lit7h.y = 178
lit7h.isVisible = false


local lit6h = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit6h.x = 438 + xOffsetToCenterCards
lit6h.y = 178
lit6h.isVisible = false


local lit6s = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit6s.x = 162 + xOffsetToCenterCards
lit6s.y = 178
lit6s.isVisible = false


local lit5h = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit5h.x = 346 + xOffsetToCenterCards
lit5h.y = 178
lit5h.isVisible = false


local lit7s = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit7s.x = 254 + xOffsetToCenterCards
lit7s.y = 178
lit7s.isVisible = false


local lit5s = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit5s.x = 70 + xOffsetToCenterCards
lit5s.y = 178
lit5s.isVisible = false	


local lit4h = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit4h.x = 530 + xOffsetToCenterCards
lit4h.y = 87
lit4h.isVisible = false


local lit3h = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit3h.x = 438 + xOffsetToCenterCards
lit3h.y = 87
lit3h.isVisible = false


local lit2h = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit2h.x = 346 + xOffsetToCenterCards
lit2h.y = 87
lit2h.isVisible = false


local lit4s = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit4s.x = 254 + xOffsetToCenterCards
lit4s.y = 87
lit4s.isVisible = false


local lit3s = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit3s.x = 162 + xOffsetToCenterCards
lit3s.y = 87
lit3s.isVisible = false


local lit2s = display.newImageRect( uiGlobals.selectedSquare, 85, 85 )
lit2s.x = 70 + xOffsetToCenterCards
lit2s.y = 87
lit2s.isVisible = false




---------------------------GUMBO LAYOUT CODE

sceneGroup:insert(litkc);
sceneGroup:insert(littc);
sceneGroup:insert(lit7c);
sceneGroup:insert(litac);
sceneGroup:insert(litqc);
sceneGroup:insert(lit9c);
sceneGroup:insert(lit6c);
sceneGroup:insert(litjc);
sceneGroup:insert(lit8c);
sceneGroup:insert(lit5c);
sceneGroup:insert(litkd);
sceneGroup:insert(littd);
sceneGroup:insert(lit7d);
sceneGroup:insert(litad);
sceneGroup:insert(litqd);
sceneGroup:insert(lit9d);
sceneGroup:insert(lit6d);
sceneGroup:insert(litjd);
sceneGroup:insert(lit8d);
sceneGroup:insert(lit4c);
sceneGroup:insert(lit5d);
sceneGroup:insert(lit3c);
sceneGroup:insert(lit2c);
sceneGroup:insert(lit4d);
sceneGroup:insert(lit3d);
sceneGroup:insert(litas);
sceneGroup:insert(litah);
sceneGroup:insert(litkh);
sceneGroup:insert(litqh);
sceneGroup:insert(litjh);
sceneGroup:insert(litqs);
sceneGroup:insert(litks);
sceneGroup:insert(litjs);
sceneGroup:insert(litts);
sceneGroup:insert(lit8h);
sceneGroup:insert(lit9h);
sceneGroup:insert(litth);
sceneGroup:insert(lit9s);
sceneGroup:insert(lit2d);
sceneGroup:insert(lit8s);
sceneGroup:insert(lit7h);
sceneGroup:insert(lit6h);
sceneGroup:insert(lit6s);
sceneGroup:insert(lit5h);
sceneGroup:insert(lit7s);
sceneGroup:insert(lit5s);
sceneGroup:insert(lit4h);
sceneGroup:insert(lit3h);
sceneGroup:insert(lit2h);
sceneGroup:insert(lit4s);
sceneGroup:insert(lit3s);
sceneGroup:insert(lit2s); 

function loadLitSquareObjectsIntoLitList()
	litList["2c"]=lit2c;
	litList["3c"]=lit3c;
	litList["4c"]=lit4c;
	litList["5c"]=lit5c;
	litList["6c"]=lit6c;
	litList["7c"]=lit7c;
	litList["8c"]=lit8c;
	litList["9c"]=lit9c;
	litList["tc"]=littc;
	litList["jc"]=litjc;
	litList["qc"]=litqc;
	litList["kc"]=litkc;
	litList["ac"]=litac;
	
	litList["2s"]=lit2s;
	litList["3s"]=lit3s;
	litList["4s"]=lit4s;
	litList["5s"]=lit5s;
	litList["6s"]=lit6s;
	litList["7s"]=lit7s;
	litList["8s"]=lit8s;
	litList["9s"]=lit9s;
	litList["ts"]=litts;
	litList["js"]=litjs;
	litList["qs"]=litqs;
	litList["ks"]=litks;
	litList["as"]=litas;
	
	litList["2h"]=lit2h;
	litList["3h"]=lit3h;
	litList["4h"]=lit4h;
	litList["5h"]=lit5h;
	litList["6h"]=lit6h;
	litList["7h"]=lit7h;
	litList["8h"]=lit8h;
	litList["9h"]=lit9h;
	litList["th"]=litth;
	litList["jh"]=litjh;
	litList["qh"]=litqh;
	litList["kh"]=litkh;
	litList["ah"]=litah;
	
	litList["2d"]=lit2d;
	litList["3d"]=lit3d;
	litList["4d"]=lit4d;
	litList["5d"]=lit5d;
	litList["6d"]=lit6d;
	litList["7d"]=lit7d;
	litList["8d"]=lit8d;
	litList["9d"]=lit9d;
	litList["td"]=littd;
	litList["jd"]=litjd;
	litList["qd"]=litqd;
	litList["kd"]=litkd;
	litList["ad"]=litad;
end  --end function loadLitSquareObjectsIntoLitList


 local function clearAllSelectedCards()
 	for pos = 1,#selectedList do
  	--print("trying to light card " .. litList[selectedList[pos]]);
 		litList[selectedList[pos]].isVisible = false
  	end
 end
 
  local function lightSelectedCards() 
  	--litList["2c"].isVisible = true
  	---[[
  		--print("selectedList[1]="..selectedList[1])
  	for pos = 1,#selectedList do
  	--print("trying to light card " .. litList[selectedList[pos]]);
  		litList[selectedList[pos]].isVisible = true
  	end
  	--]]
  end
  
local function onButtonRelease( event )
	print("---previously selected hole cards should have value here---")
		for k, v in pairs( previousSelectedHoleCards ) do
   			print(k, v)
		end
		print("---previously selected hole cards should have value here---")
	local btn = event.target  --grab the button that was pressed
	if params.soundOn == true then
		local buttonClickSoundChannel = audio.play(buttonClickSound);
	end
		
	local finalCardString = ""
	-- check to see if ok or cancel was pressed
	params.alreadySelectedCards = selectedList
	if btn.id == "ok" then
		--check to see number of cards cards selected 
		print (#selectedList .. " items in litList".."  number of cards to return "..params.numcardstochoose );
		if #selectedList == params.numcardstochoose then
			print("ok to return");
			--break up two cards into their value and suite
			--selectedList[1] is first card like "Ad"  get substring 1 for value and substring 2 for suit
			if #selectedList == 1 then --turn card
				print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! figure out turn card value")
				params.selectedTurnCard = selectedList   --save turn card as the ones currently selected
				
				local card1Value = string.sub(selectedList[1],1,1);
				local card1Suit = string.sub(selectedList[1],2,2);
				print("card1value = " .. card1Value .. " card1Suit = ".. card1Suit);
				finalCardString = card1Value.."s"
				params.turnCard = card1Value..card1Suit
				params.turnCardValue = card1Value
				
				composer.hideOverlay( uiGlobals.sceneTransitionType, uiGlobals.sceneTransitionTime )
				--composer.gotoScene( "mainscreen" ,options)
				--director:changeScene(params,"mainScreen","downFlip"); 
				--play scene switch sound if sound on
				if params.soundOn == true then
					local buttonSceneSoundChannel = audio.play(switchSceneSound);
				end
				return
		end --if #selectedList == 2
			if #selectedList == 2 then --hole cards
				params.selectedHoleCards = selectedList   --save hole cards as the ones currently selected
				local card1Value = string.sub(selectedList[1],1,1);
				local card1Suit = string.sub(selectedList[1],2,2);
				local card2Value = string.sub(selectedList[2],1,1);
				local card2Suit = string.sub(selectedList[2],2,2);
				print("card1value = " .. card1Value .. " card1Suit = ".. card1Suit);
				print("card2value = " .. card2Value .. " card2Suit = ".. card2Suit);
		
				--[[if cardRank[card1Value] > cardRank[card2Value] then
					finalCardString = card1Value .. card2Value
					params.card1 = card1Value .. card1Suit  --make card 1 the bigger card
					params.card2=card2Value.. card2Suit
				elseif cardRank[card1Value]  == cardRank[card2Value] then
					params.holePocketPair = true  --pockepair
				end
				--else
					finalCardString = card2Value .. card1Value
					params.card1 = card2Value .. card2Suit  --make card 1 the bigger card
					params.card2=card1Value.. card1Suit
			--]]
			
				if cardRank[card1Value]  == cardRank[card2Value] then
					params.holePocketPair = true  --pockepair
				else
					params.holePocketPair = false  --not pockepair
				end
				
					finalCardString = card2Value .. card1Value
					params.card1=card1Value.. card1Suit
					params.card1Value=card1Value
					params.card2 = card2Value .. card2Suit  
					params.card2Value=card2Value
	
					
				--check for suitedness
				if card1Suit == card2Suit then
					print("suited")
					params.holeSuited = true
					finalCardString = finalCardString .."s"
				else
					print("unsuited")
					params.holeSuited = false
					finalCardString = finalCardString .."o"
				end   --if same suit
				print("finalCardString = " .. finalCardString); 
				params.hand = finalCardString;  --in format ValueValueSuitedness eg: A4S
			
				--CHECK to see if 2 cards are in any straight
				params.twoToStraight = false -- assume no 2 to straight
				local tempStraightTable = {}
				for _,v in pairs(ot.possibleStraights) do
					print("v=" ..v .."   params.card1Value= " .. card1Value)
					if string.find(v,card1Value) then
						print("*************************FOUND CARD 1 in a straight")
						table.insert(tempStraightTable,v)
					--if v == "orange" then
						-- do something
						--break
					end
				end
				--see if card 2 is in the possible straights from card 1
				for _,v in pairs(tempStraightTable) do
					print("v=" ..v .."   params.card2Value= " .. card2Value)
					if string.find(v,card2Value) then
						print("*************************FOUND CARD 2 in a straight")
						print("SETTING params.twoToStraight to TRUE")
						params.twoToStraight = true
						break
					end
				end
				print("params.twoToStraight=".. tostring(params.twoToStraight))
				composer.hideOverlay( uiGlobals.sceneTransitionType, uiGlobals.sceneTransitionTime )
				--composer.gotoScene( "mainscreen" ,options)
				--director:changeScene(params,"mainScreen","downFlip"); 
				--play scene switch sound if sound on
				if params.soundOn == true then
					local buttonSceneSoundChannel = audio.play(switchSceneSound);
				end
				return
		end --if #selectedList == 2
		if #selectedList == 3 then --flop cards
				params.selectedFlopCards = selectedList   --save the selected cards in flop list
				local card1Value = string.sub(selectedList[1],1,1);
				local card1Suit = string.sub(selectedList[1],2,2);
				local card2Value = string.sub(selectedList[2],1,1);
				local card2Suit = string.sub(selectedList[2],2,2);
				local card3Value = string.sub(selectedList[3],1,1);
				local card3Suit = string.sub(selectedList[3],2,2);
				print("card1value = " .. card1Value .. " card1Suit = ".. card1Suit);
				print("card2value = " .. card2Value .. " card2Suit = ".. card2Suit);
				print("card3value = " .. card3Value .. " card3Suit = ".. card3Suit);
		
				params.flopCard1=card1Value .. card1Suit
				params.flopCard1Value = card1Value
				params.flopCard2=card2Value .. card2Suit
				params.flopCard2Value = card2Value
				params.flopCard3=card3Value .. card3Suit
				params.flopCard3Value = card3Value			
			
				--check for suitedness
				if card1Suit == card2Suit then
					print("suited")
					finalCardString = finalCardString .."s"
				else
					print("unsuited")
					finalCardString = finalCardString .."o"
				end   --if same suit
				print("finalCardString = " .. finalCardString); 
				params.hand = finalCardString;  --in format ValueValueSuitedness eg: A4S
			
				composer.hideOverlay( uiGlobals.sceneTransitionType, uiGlobals.sceneTransitionTime )
				--composer.gotoScene( "mainscreen" ,options)
				--director:changeScene(params,"mainScreen","downFlip"); 
				--play scene switch sound if sound on
				if params.soundOn == true then
					local buttonSceneSoundChannel = audio.play(switchSceneSound);
				end
				return
		end --if #selectedList == 3
			
		else   --wrong number of cards selected
			local alert = native.showAlert( "Incorrect Number of Cards Selected", "You must choose exactly " .. params.numcardstochoose .. " cards.",{"OK"})
			print("not ok to return")
			return
		end

	elseif btn.id == "cancel" then 
		print("---cancel-setting previously selected hole cards to this---")
		for k, v in pairs( previousSelectedHoleCards ) do
   			print(k, v)
		end
		print("---cancel-setting previously selected hole cards to this---")
		--params.selectedHoleCards = previousSelectedHoleCards --leave initially selected cards unchanged
		--params.selectedFlopCards = previousSelectedFlopCards --leave initially selected cards unchanged
		--params.selectedTurnCard = previousSelectedTurnCard --leave initially selected cards unchanged

		params.selectedHoleCards = shallowCopy(previousSelectedHoleCards)  --leave initially selected cards unchanged
		params.selectedFlopCards = shallowCopy(previousSelectedFlopCards) --leave initially selected cards unchanged
		params.selectedTurnCard = shallowCopy(previousSelectedTurnCard) --leave initially selected cards unchanged

		
		--[[
		local options = {
		 	effect = "fade",
		  	time = 500,
		  	params = params
		}
		--]]
		--composer.gotoScene( "mainscreen" ,options)
		composer.hideOverlay(uiGlobals.sceneTransitionType, uiGlobals.sceneTransitionTime)
		--director:changeScene(params,"mainScreen","downFlip");
		if params.soundOn == true then
			local buttonSceneSoundChannel = audio.play(switchSceneSound);
		end
		return
	elseif (btn.id == "helpcardchooser") then  --show hand info help
		print("CardChooserHelpText=" .. helpText.CardChooserHelpText)
		local alert = native.showAlert( "Card Chooser Help", helpText.CardChooserHelpText ,{"OK"})
		return
	end
	clearAllSelectedCards();
    
    print( "User has pressed and released the button with id: " .. btn.id )
  
   local notInTable = true;
   local positionOfItemInTable;
    for pos = 1,#selectedList do
        if selectedList[pos] == btn.id then
        -- do something
        positionOfItemInTable = pos
        notInTable = false;
         print("already in table")
         break
        end
   end
   -- if not in the table of cards, then add it to the table of selected cards
   if notInTable == true then
   	table.insert(selectedList,btn.id);
      --  selectedList[#selectedList+1]= btn.id
    	print("num items in selected list: " .. #selectedList)
    	for i=1,# selectedList do
     	print(i,selectedList[i])
    	end
    else  -- remove card from table
    print("pos=" ..positionOfItemInTable);
    	table.remove ( selectedList, positionOfItemInTable)
   end --end if notInTable
   -- show selected cards
     lightSelectedCards()

end
local helpCardChooser= widget.newButton{
    id = "helpcardchooser",
    defaultFile = "images/help-button.png",
    overFile = "images/help-button-pressed.png",
    width = 65, 
    height = 65,
    onRelease = onButtonRelease
}
helpCardChooser.x = helpCardChooserx
helpCardChooser.y = helpCardChoosery
helpCardChooser.isVisible = false
sceneGroup:insert(helpCardChooser)

-----------------------------------card buttons
-----************************************CLUBS BUTTONS BEGIN***********************************
local button2c = widget.newButton{
    id = "2c",
    defaultFile = "images/cardbuttons/2cbutton.png",
    overFile = "images/cardbuttons/2cbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button2c.x=346 + xOffsetToCenterCards;
button2c.y=546;
sceneGroup:insert(button2c);

local button3c = widget.newButton{
    id = "3c",
    defaultFile = "images/cardbuttons/3cbutton.png",
    overFile = "images/cardbuttons/3cbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button3c.x=438 + xOffsetToCenterCards;
button3c.y=546;
sceneGroup:insert(button3c);

local button4c = widget.newButton{
    id = "4c",
    defaultFile = "images/cardbuttons/4cbutton.png",
    overFile = "images/cardbuttons/4cbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button4c.x = 530 + xOffsetToCenterCards
button4c.y = 546
sceneGroup:insert(button4c);

local button5c = widget.newButton{
    id = "5c",
    defaultFile = "images/cardbuttons/5cbutton.png",
    overFile = "images/cardbuttons/5cbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button5c.x = 346 + xOffsetToCenterCards
button5c.y = 638
sceneGroup:insert(button5c);

local button6c = widget.newButton{
    id = "6c",
    defaultFile = "images/cardbuttons/6cbutton.png",
    overFile = "images/cardbuttons/6cbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button6c.x = 438 + xOffsetToCenterCards
button6c.y = 638
sceneGroup:insert(button6c);

local button7c = widget.newButton{
    id = "7c",
    defaultFile = "images/cardbuttons/7cbutton.png",
    overFile = "images/cardbuttons/7cbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button7c.x = 530 + xOffsetToCenterCards
button7c.y = 638
sceneGroup:insert(button7c);

local button8c = widget.newButton{
    id = "8c",
    defaultFile = "images/cardbuttons/8cbutton.png",
    overFile = "images/cardbuttons/8cbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button8c.x = 346 + xOffsetToCenterCards
button8c.y = 730
sceneGroup:insert(button8c);

local button9c = widget.newButton{
    id = "9c",
    defaultFile = "images/cardbuttons/9cbutton.png",
    overFile = "images/cardbuttons/9cbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button9c.x = 438 + xOffsetToCenterCards
button9c.y = 730
sceneGroup:insert(button9c);

local buttontc = widget.newButton{
    id = "tc",
    defaultFile = "images/cardbuttons/tcbutton.png",
    overFile = "images/cardbuttons/tcbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
buttontc.x = 530 + xOffsetToCenterCards
buttontc.y = 730
sceneGroup:insert(buttontc);

local buttonjc = widget.newButton{
    id = "jc",
    defaultFile = "images/cardbuttons/jcbutton.png",
    overFile = "images/cardbuttons/jcbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
buttonjc.x = 347 + xOffsetToCenterCards
buttonjc.y = 822
sceneGroup:insert(buttonjc);

local buttonqc = widget.newButton{
    id = "qc",
    defaultFile = "images/cardbuttons/qcbutton.png",
    overFile = "images/cardbuttons/qcbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
buttonqc.x = 438 + xOffsetToCenterCards
buttonqc.y = 822
sceneGroup:insert(buttonqc);

local buttonkc = widget.newButton{
    id = "kc",
    defaultFile = "images/cardbuttons/kcbutton.png",
    overFile = "images/cardbuttons/kcbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
buttonkc.x = 530 + xOffsetToCenterCards
buttonkc.y = 822
sceneGroup:insert(buttonkc);

local buttonac = widget.newButton{
    id = "ac",
    defaultFile = "images/cardbuttons/acbutton.png",
    overFile = "images/cardbuttons/acbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
buttonac.x = 530 + xOffsetToCenterCards
buttonac.y = 914
sceneGroup:insert(buttonac);
-----************************************CLUBS BUTTONS END***********************************
-----************************************HEART BUTTONS BEGIN***********************************
local button2h = widget.newButton{
    id = "2h",
    defaultFile = "images/cardbuttons/2hbutton.png",
    overFile = "images/cardbuttons/2hbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button2h.x = 346 + xOffsetToCenterCards
button2h.y = 87
sceneGroup:insert(button2h);

local button3h = widget.newButton{
    id = "3h",
    defaultFile = "images/cardbuttons/3hbutton.png",
    overFile = "images/cardbuttons/3hbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button3h.x = 439 + xOffsetToCenterCards
button3h.y = 87
sceneGroup:insert(button3h);

local button4h = widget.newButton{
    id = "4h",
    defaultFile = "images/cardbuttons/4hbutton.png",
    overFile = "images/cardbuttons/4hbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button4h.x = 530 + xOffsetToCenterCards
button4h.y = 87
sceneGroup:insert(button4h);

local button5h = widget.newButton{
    id = "5h",
    defaultFile = "images/cardbuttons/5hbutton.png",
    overFile = "images/cardbuttons/5hbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button5h.x = 346 + xOffsetToCenterCards
button5h.y = 178
sceneGroup:insert(button5h);
 
local button6h = widget.newButton{
    id = "6h",
    defaultFile = "images/cardbuttons/6hbutton.png",
    overFile = "images/cardbuttons/6hbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button6h.x = 438 + xOffsetToCenterCards
button6h.y = 178
sceneGroup:insert(button6h);

local button7h = widget.newButton{
    id = "7h",
    defaultFile = "images/cardbuttons/7hbutton.png",
    overFile = "images/cardbuttons/7hbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button7h.x = 530 + xOffsetToCenterCards
button7h.y = 178
sceneGroup:insert(button7h);

local button8h = widget.newButton{
    id = "8h",
    defaultFile = "images/cardbuttons/8hbutton.png",
    overFile = "images/cardbuttons/8hbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button8h.x = 346 + xOffsetToCenterCards
button8h.y = 270
sceneGroup:insert(button8h);

local button9h = widget.newButton{
    id = "9h",
    defaultFile = "images/cardbuttons/9hbutton.png",
    overFile = "images/cardbuttons/9hbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button9h.x = 439 + xOffsetToCenterCards
button9h.y = 270
sceneGroup:insert(button9h);

local buttonth = widget.newButton{
    id = "th",
    defaultFile = "images/cardbuttons/thbutton.png",
    overFile = "images/cardbuttons/thbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
buttonth.x = 530 + xOffsetToCenterCards
buttonth.y = 270
sceneGroup:insert(buttonth);

local buttonjh = widget.newButton{
    id = "jh",
    defaultFile = "images/cardbuttons/jhbutton.png",
    overFile = "images/cardbuttons/jhbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
buttonjh.x = 346 + xOffsetToCenterCards
buttonjh.y = 362
sceneGroup:insert(buttonjh);

local buttonqh = widget.newButton{
    id = "qh",
    defaultFile = "images/cardbuttons/qhbutton.png",
    overFile = "images/cardbuttons/qhbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
buttonqh.x = 438 + xOffsetToCenterCards
buttonqh.y = 362
sceneGroup:insert(buttonqh);

local buttonkh = widget.newButton{
    id = "kh",
    defaultFile = "images/cardbuttons/khbutton.png",
    overFile = "images/cardbuttons/khbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
buttonkh.x = 530 + xOffsetToCenterCards
buttonkh.y = 362
sceneGroup:insert(buttonkh);

local buttonah = widget.newButton{
    id = "ah",
    defaultFile = "images/cardbuttons/ahbutton.png",
    overFile = "images/cardbuttons/ahbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
buttonah.x = 530 + xOffsetToCenterCards
buttonah.y = 454
sceneGroup:insert(buttonah);
-----************************************HEART BUTTONS END***********************************
-----************************************SPADE BUTTONS BEGIN***********************************
local button2s = widget.newButton{
    id = "2s",
    defaultFile = "images/cardbuttons/2sbutton.png",
    overFile = "images/cardbuttons/2sbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button2s.x = 70 + xOffsetToCenterCards
button2s.y = 87
sceneGroup:insert(button2s);

local button3s = widget.newButton{
    id = "3s",
    defaultFile = "images/cardbuttons/3sbutton.png",
    overFile = "images/cardbuttons/3sbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button3s.x = 162 + xOffsetToCenterCards
button3s.y = 87
sceneGroup:insert(button3s);

local button4s = widget.newButton{
    id = "4s",
    defaultFile = "images/cardbuttons/4sbutton.png",
    overFile = "images/cardbuttons/4sbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button4s.x = 254 + xOffsetToCenterCards
button4s.y = 87
sceneGroup:insert(button4s);

local button5s = widget.newButton{
    id = "5s",
    defaultFile = "images/cardbuttons/5sbutton.png",
    overFile = "images/cardbuttons/5sbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button5s.x = 70 + xOffsetToCenterCards
button5s.y = 178
sceneGroup:insert(button5s);

local button6s = widget.newButton{
    id = "6s",
    defaultFile = "images/cardbuttons/6sbutton.png",
    overFile = "images/cardbuttons/6sbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button6s.x = 162 + xOffsetToCenterCards
button6s.y = 178
sceneGroup:insert(button6s);

local button7s = widget.newButton{
    id = "7s",
    defaultFile = "images/cardbuttons/7sbutton.png",
    overFile = "images/cardbuttons/7sbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button7s.x = 254 + xOffsetToCenterCards
button7s.y = 178
sceneGroup:insert(button7s);

local button8s = widget.newButton{
    id = "8s",
    defaultFile = "images/cardbuttons/8sbutton.png",
    overFile = "images/cardbuttons/8sbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button8s.x = 70 + xOffsetToCenterCards
button8s.y = 270
sceneGroup:insert(button8s);

local button9s = widget.newButton{
    id = "9s",
    defaultFile = "images/cardbuttons/9sbutton.png",
    overFile = "images/cardbuttons/9sbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button9s.x = 162 + xOffsetToCenterCards
button9s.y = 270
sceneGroup:insert(button9s);

local buttonts = widget.newButton{
    id = "ts",
    defaultFile = "images/cardbuttons/tsbutton.png",
    overFile = "images/cardbuttons/tsbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
buttonts.x = 254 + xOffsetToCenterCards
buttonts.y = 270
sceneGroup:insert(buttonts);

local buttonjs = widget.newButton{
    id = "js",
    defaultFile = "images/cardbuttons/jsbutton.png",
    overFile = "images/cardbuttons/jsbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
buttonjs.x = 70 + xOffsetToCenterCards
buttonjs.y = 362
sceneGroup:insert(buttonjs);

local buttonqs = widget.newButton{
    id = "qs",
    defaultFile = "images/cardbuttons/qsbutton.png",
    overFile = "images/cardbuttons/qsbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
buttonqs.x = 162 + xOffsetToCenterCards
buttonqs.y = 362
sceneGroup:insert(buttonqs);

local buttonks = widget.newButton{
    id = "ks",
    defaultFile = "images/cardbuttons/ksbutton.png",
    overFile = "images/cardbuttons/ksbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
buttonks.x = 254 + xOffsetToCenterCards
buttonks.y = 362
sceneGroup:insert(buttonks);

local buttonas = widget.newButton{
    id = "as",
    defaultFile = "images/cardbuttons/asbutton.png",
    overFile = "images/cardbuttons/asbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
buttonas.x = 70 + xOffsetToCenterCards
buttonas.y = 454
sceneGroup:insert(buttonas);

-----************************************SPADE BUTTONS END***********************************
-----************************************DIAMOND BUTTONS BEGIN***********************************
local button2d = widget.newButton{
    id = "2d",
    defaultFile = "images/cardbuttons/2dbutton.png",
    overFile = "images/cardbuttons/2dbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button2d.x = 70 + xOffsetToCenterCards
button2d.y = 546
sceneGroup:insert(button2d);

local button3d = widget.newButton{
    id = "3d",
    defaultFile = "images/cardbuttons/3dbutton.png",
    overFile = "images/cardbuttons/3dbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button3d.x = 162 + xOffsetToCenterCards
button3d.y = 546
sceneGroup:insert(button3d);

local button4d = widget.newButton{
    id = "4d",
    defaultFile = "images/cardbuttons/4dbutton.png",
    overFile = "images/cardbuttons/4dbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button4d.x = 254 + xOffsetToCenterCards
button4d.y = 546
sceneGroup:insert(button4d);

local button5d = widget.newButton{
    id = "5d",
    defaultFile = "images/cardbuttons/5dbutton.png",
    overFile = "images/cardbuttons/5dbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button5d.x = 70 + xOffsetToCenterCards
button5d.y = 638
sceneGroup:insert(button5d);

local button6d = widget.newButton{
    id = "6d",
    defaultFile = "images/cardbuttons/6dbutton.png",
    overFile = "images/cardbuttons/6dbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button6d.x = 162 + xOffsetToCenterCards
button6d.y = 638
sceneGroup:insert(button6d);

local button7d = widget.newButton{
    id = "7d",
    defaultFile = "images/cardbuttons/7dbutton.png",
    overFile = "images/cardbuttons/7dbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button7d.x = 254 + xOffsetToCenterCards
button7d.y = 638
sceneGroup:insert(button7d);

local button8d = widget.newButton{
    id = "8d",
    defaultFile = "images/cardbuttons/8dbutton.png",
    overFile = "images/cardbuttons/8dbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button8d.x = 70 + xOffsetToCenterCards
button8d.y = 730
sceneGroup:insert(button8d);

local button9d = widget.newButton{
    id = "9d",
    defaultFile = "images/cardbuttons/9dbutton.png",
    overFile = "images/cardbuttons/9dbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
button9d.x = 162 + xOffsetToCenterCards
button9d.y = 730
sceneGroup:insert(button9d);

local buttontd = widget.newButton{
    id = "td",
    defaultFile = "images/cardbuttons/tdbutton.png",
    overFile = "images/cardbuttons/tdbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
buttontd.x = 254 + xOffsetToCenterCards
buttontd.y = 730
sceneGroup:insert(buttontd);

local buttonjd = widget.newButton{
    id = "jd",
    defaultFile = "images/cardbuttons/jdbutton.png",
    overFile = "images/cardbuttons/jdbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
buttonjd.x = 70 + xOffsetToCenterCards
buttonjd.y = 822
sceneGroup:insert(buttonjd);

local buttonqd = widget.newButton{
    id = "qd",
    defaultFile = "images/cardbuttons/qdbutton.png",
    overFile = "images/cardbuttons/qdbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
buttonqd.x = 162 + xOffsetToCenterCards
buttonqd.y = 822
sceneGroup:insert(buttonqd);

local buttonkd = widget.newButton{
    id = "kd",
    defaultFile = "images/cardbuttons/kdbutton.png",
    overFile = "images/cardbuttons/kdbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
buttonkd.x = 254 + xOffsetToCenterCards
buttonkd.y = 822
sceneGroup:insert(buttonkd);

local buttonad = widget.newButton{
    id = "ad",
    defaultFile = "images/cardbuttons/adbutton.png",
    overFile = "images/cardbuttons/adbutton-pressed.png",
    width = 80, 
    height = 79,
    onRelease = onButtonRelease
}
buttonad.x = 70 + xOffsetToCenterCards
buttonad.y = 915
sceneGroup:insert(buttonad);

-----------------------------------card buttons

function loadCardObjectsIntoList()
	cardList["2c"]=button2c
	cardList["3c"]=button3c
	cardList["4c"]=button4c
	cardList["5c"]=button5c
	cardList["6c"]=button6c
	cardList["7c"]=button7c
	cardList["8c"]=button8c
	cardList["9c"]=button9c
	cardList["tc"]=buttontc
	cardList["jc"]=buttonjc
	cardList["qc"]=buttonqc
	cardList["kc"]=buttonkc
	cardList["ac"]=buttonac
	
	cardList["2s"]=button2s
	cardList["3s"]=button3s
	cardList["4s"]=button4s
	cardList["5s"]=button5s
	cardList["6s"]=button6s
	cardList["7s"]=button7s
	cardList["8s"]=button8s
	cardList["9s"]=button9s
	cardList["ts"]=buttonts
	cardList["js"]=buttonjs
	cardList["qs"]=buttonqs
	cardList["ks"]=buttonks
	cardList["as"]=buttonas
	
	cardList["2h"]=button2h
	cardList["3h"]=button3h
	cardList["4h"]=button4h
	cardList["5h"]=button5h
	cardList["6h"]=button6h
	cardList["7h"]=button7h
	cardList["8h"]=button8h
	cardList["9h"]=button9h
	cardList["th"]=buttonth
	cardList["jh"]=buttonjh
	cardList["qh"]=buttonqh
	cardList["kh"]=buttonkh
	cardList["ah"]=buttonah
	
	cardList["2d"]=button2d
	cardList["3d"]=button3d
	cardList["4d"]=button4d
	cardList["5d"]=button5d
	cardList["6d"]=button6d
	cardList["7d"]=button7d
	cardList["8d"]=button8d
	cardList["9d"]=button9d
	cardList["td"]=buttontd
	cardList["jd"]=buttonjd
	cardList["qd"]=buttonqd
	cardList["kd"]=buttonkd
	cardList["ad"]=buttonad
end 

--********************************************************************************
-- function hideAlreadySelectedCards()
--********************************************************************************
function hideAlreadySelectedCards()
	if params.numcardstochoose == 3  then --choosing flop so hide the hole cards and turn card
		for pos = 1,#params.selectedHoleCards do
			cardList[params.selectedHoleCards[pos]].isVisible = false
		end
		for pos = 1,#params.selectedTurnCard do
			cardList[params.selectedTurnCard[pos]].isVisible = false
		end
	elseif params.numcardstochoose == 2  then --choosing hole so hide the flop cards and turn card
		for pos = 1,#params.selectedFlopCards do
			cardList[params.selectedFlopCards[pos]].isVisible = false
		end
		for pos = 1,#params.selectedTurnCard do
			cardList[params.selectedTurnCard[pos]].isVisible = false
		end
	elseif params.numcardstochoose == 1  then --choosing turn so hide the flop cards and hole cards
		for pos = 1,#params.selectedFlopCards do
			cardList[params.selectedFlopCards[pos]].isVisible = false
		end
		for pos = 1,#params.selectedHoleCards do
			cardList[params.selectedHoleCards[pos]].isVisible = false
		end
	end  --end if/elseif
end   --end hideAlreadySelectedCards()

-- ******play scene switch sound when the screen is loaded
if params.soundOn == true then
		local buttonSceneSoundChannel = audio.play(switchSceneSound);
end

------------------------------------click directions and ok and cancel buttons
local directions1 = display.newText(directionsText1.." "..params.numcardstochoose,directionsText1X, directionsText1Y,native.systemFont,directionsFontSize)
directions1:setFillColor(250, 255, 12)
sceneGroup:insert(directions1);
local chooseType
if params.numcardstochoose == 1 then --choose turn card
	chooseType = "Turn "
    directionsText2 = "Card"   --choose single card instead of cards
elseif params.numcardstochoose == 2 then  --choose hole cards
	chooseType = "Hole "
elseif params.numcardstochoose == 3 then  --choose flop cards
	chooseType =  "Flop "
end

local directions2 = display.newText(chooseType .. directionsText2,directionsText2X, directionsText2Y,native.systemFont,directionsFontSize)
directions2:setFillColor(250, 255, 12)
sceneGroup:insert(directions2);

local buttonok = widget.newButton{
    id = "ok",
    defaultFile = "images/okbutton.png",
    overFile = "images/okbutton-pressed.png",
    width = 100, 
    height =100,
    onRelease = onButtonRelease
}
buttonok.x = 194 + xOffsetToCenterCards
buttonok.y = 960
sceneGroup:insert(buttonok);

local buttoncancel = widget.newButton{
    id = "cancel",
    defaultFile = "images/cancelbutton.png",
    overFile = "images/cancelbutton-pressed.png",
    width = 100, 
    height = 100,
    onRelease = onButtonRelease
}
buttoncancel.x = 394 + xOffsetToCenterCards
buttoncancel.y = 960
sceneGroup:insert(buttoncancel);
------------------------------------ok and cancel buttons

-------------------------------start here
loadLitSquareObjectsIntoLitList()
loadCardObjectsIntoList()
if params.numcardstochoose == 1 then   --turn
	selectedList = params.selectedTurnCard
	elseif params.numcardstochoose == 2 then   --hole cards
		selectedList = params.selectedHoleCards
	elseif params.numcardstochoose == 3 then   --flop
		selectedList = params.selectedFlopCards
end   --end if
lightSelectedCards()
hideAlreadySelectedCards()



-----************************************DIAMOND BUTTONS END***********************************

end   --end scene:create

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
	end	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	local parent = event.parent  --reference to the parent scene object
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
		
		--combine all cards for completeHand
		params.completeHand = params.card1 .. params.card2 .. params.flopCard1 .. params.flopCard2 .. params.flopCard3 .. params.turnCard
		--save the selected cards in alreadySelectedCards table
		
		parent:BackFromChoosingCards()
			
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
end


function scene:destroy( event )
	local sceneGroup = self.view
	
	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:02d9ef74fda1596f8b3191971ec57d87:06b14bd9585f5e051b185c45b7520a72:ee2dace2a2b8f2c5a5bdb74dbe27239f$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- 2cbutton-pressed
            x=1684,
            y=326,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 2cbutton
            x=1684,
            y=245,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 2dbutton-pressed
            x=1604,
            y=326,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 2dbutton
            x=1604,
            y=245,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 2hbutton-pressed
            x=827,
            y=164,
            width=80,
            height=79,

        },
        {
            -- 2hbutton
            x=745,
            y=164,
            width=80,
            height=79,

        },
        {
            -- 2sbutton-pressed
            x=663,
            y=164,
            width=80,
            height=79,

        },
        {
            -- 2sbutton
            x=581,
            y=164,
            width=80,
            height=79,

        },
        {
            -- 3cbutton-pressed
            x=1524,
            y=326,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 3cbutton
            x=1524,
            y=245,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 3dbutton-pressed
            x=1444,
            y=326,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 3dbutton
            x=1444,
            y=245,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 3hbutton-pressed
            x=499,
            y=164,
            width=80,
            height=79,

        },
        {
            -- 3hbutton
            x=417,
            y=164,
            width=80,
            height=79,

        },
        {
            -- 3sbutton-pressed
            x=335,
            y=164,
            width=80,
            height=79,

        },
        {
            -- 3sbutton
            x=253,
            y=164,
            width=80,
            height=79,

        },
        {
            -- 4cbutton-pressed
            x=1364,
            y=326,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 4cbutton
            x=1364,
            y=245,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 4dbutton-pressed
            x=1284,
            y=326,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 4dbutton
            x=1284,
            y=245,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 4hbutton-pressed
            x=171,
            y=164,
            width=80,
            height=79,

        },
        {
            -- 4hbutton
            x=89,
            y=164,
            width=80,
            height=79,

        },
        {
            -- 4sbutton-pressed
            x=1647,
            y=83,
            width=80,
            height=79,

        },
        {
            -- 4sbutton
            x=1647,
            y=2,
            width=80,
            height=79,

        },
        {
            -- 5cbutton-pressed
            x=1204,
            y=326,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 5cbutton
            x=1204,
            y=245,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 5dbutton-pressed
            x=1124,
            y=326,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 5dbutton
            x=1124,
            y=245,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 5hbutton-pressed
            x=1565,
            y=83,
            width=80,
            height=79,

        },
        {
            -- 5hbutton
            x=1565,
            y=2,
            width=80,
            height=79,

        },
        {
            -- 5sbutton-pressed
            x=1483,
            y=83,
            width=80,
            height=79,

        },
        {
            -- 5sbutton
            x=1483,
            y=2,
            width=80,
            height=79,

        },
        {
            -- 6cbutton-pressed
            x=1044,
            y=326,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 6cbutton
            x=1044,
            y=245,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 6dbutton-pressed
            x=964,
            y=326,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 6dbutton
            x=964,
            y=245,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 6hbutton-pressed
            x=1401,
            y=83,
            width=80,
            height=79,

        },
        {
            -- 6hbutton
            x=1401,
            y=2,
            width=80,
            height=79,

        },
        {
            -- 6sbutton-pressed
            x=1319,
            y=83,
            width=80,
            height=79,

        },
        {
            -- 6sbutton
            x=1319,
            y=2,
            width=80,
            height=79,

        },
        {
            -- 7cbutton-pressed
            x=884,
            y=326,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 7cbutton
            x=884,
            y=245,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 7dbutton-pressed
            x=804,
            y=326,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 7dbutton
            x=804,
            y=245,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 7hbutton-pressed
            x=1237,
            y=83,
            width=80,
            height=79,

        },
        {
            -- 7hbutton
            x=1237,
            y=2,
            width=80,
            height=79,

        },
        {
            -- 7sbutton-pressed
            x=1155,
            y=83,
            width=80,
            height=79,

        },
        {
            -- 7sbutton
            x=1155,
            y=2,
            width=80,
            height=79,

        },
        {
            -- 8cbutton-pressed
            x=724,
            y=326,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 8cbutton
            x=724,
            y=245,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 8dbutton-pressed
            x=644,
            y=326,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 8dbutton
            x=644,
            y=245,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 8hbutton-pressed
            x=1073,
            y=83,
            width=80,
            height=79,

        },
        {
            -- 8hbutton
            x=1073,
            y=2,
            width=80,
            height=79,

        },
        {
            -- 8sbutton-pressed
            x=991,
            y=83,
            width=80,
            height=79,

        },
        {
            -- 8sbutton
            x=991,
            y=2,
            width=80,
            height=79,

        },
        {
            -- 9cbutton-pressed
            x=564,
            y=326,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 9cbutton
            x=564,
            y=245,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 9dbutton-pressed
            x=484,
            y=326,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 9dbutton
            x=484,
            y=245,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- 9hbutton-pressed
            x=909,
            y=83,
            width=80,
            height=79,

        },
        {
            -- 9hbutton
            x=909,
            y=2,
            width=80,
            height=79,

        },
        {
            -- 9sbutton-pressed
            x=827,
            y=83,
            width=80,
            height=79,

        },
        {
            -- 9sbutton
            x=827,
            y=2,
            width=80,
            height=79,

        },
        {
            -- acbutton-pressed
            x=404,
            y=326,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- acbutton
            x=404,
            y=245,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- adbutton-pressed
            x=324,
            y=326,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- adbutton
            x=324,
            y=245,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- ahbutton-pressed
            x=745,
            y=83,
            width=80,
            height=79,

        },
        {
            -- ahbutton
            x=745,
            y=2,
            width=80,
            height=79,

        },
        {
            -- asbutton-pressed
            x=663,
            y=83,
            width=80,
            height=79,

        },
        {
            -- asbutton
            x=663,
            y=2,
            width=80,
            height=79,

        },
        {
            -- jcbutton-pressed
            x=244,
            y=326,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- jcbutton
            x=244,
            y=245,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- jdbutton-pressed
            x=164,
            y=326,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- jdbutton
            x=164,
            y=245,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- jhbutton-pressed
            x=581,
            y=83,
            width=80,
            height=79,

        },
        {
            -- jhbutton
            x=581,
            y=2,
            width=80,
            height=79,

        },
        {
            -- jsbutton-pressed
            x=499,
            y=83,
            width=80,
            height=79,

        },
        {
            -- jsbutton
            x=499,
            y=2,
            width=80,
            height=79,

        },
        {
            -- kcbutton-pressed
            x=84,
            y=326,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- kcbutton
            x=84,
            y=245,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- kdbutton-pressed
            x=1629,
            y=164,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- kdbutton
            x=1549,
            y=164,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- khbutton-pressed
            x=417,
            y=83,
            width=80,
            height=79,

        },
        {
            -- khbutton
            x=417,
            y=2,
            width=80,
            height=79,

        },
        {
            -- ksbutton-pressed
            x=335,
            y=83,
            width=80,
            height=79,

        },
        {
            -- ksbutton
            x=335,
            y=2,
            width=80,
            height=79,

        },
        {
            -- qcbutton-pressed
            x=1469,
            y=164,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- qcbutton
            x=1389,
            y=164,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- qdbutton-pressed
            x=1309,
            y=164,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- qdbutton
            x=1229,
            y=164,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- qhbutton-pressed
            x=253,
            y=83,
            width=80,
            height=79,

        },
        {
            -- qhbutton
            x=253,
            y=2,
            width=80,
            height=79,

        },
        {
            -- qsbutton-pressed
            x=171,
            y=83,
            width=80,
            height=79,

        },
        {
            -- qsbutton
            x=2,
            y=257,
            width=80,
            height=79,

        },
        {
            -- selectedsquare-yellow
            x=2,
            y=89,
            width=85,
            height=85,

        },
        {
            -- selectedsquare
            x=2,
            y=2,
            width=85,
            height=85,

        },
        {
            -- tcbutton-pressed
            x=1149,
            y=164,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- tcbutton
            x=1069,
            y=164,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- tdbutton-pressed
            x=989,
            y=164,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- tdbutton
            x=909,
            y=164,
            width=78,
            height=79,

            sourceX = 1,
            sourceY = 0,
            sourceWidth = 80,
            sourceHeight = 79
        },
        {
            -- thbutton-pressed
            x=171,
            y=2,
            width=80,
            height=79,

        },
        {
            -- thbutton
            x=89,
            y=83,
            width=80,
            height=79,

        },
        {
            -- tsbutton-pressed
            x=2,
            y=176,
            width=80,
            height=79,

        },
        {
            -- tsbutton
            x=89,
            y=2,
            width=80,
            height=79,

        },
    },
    
    sheetContentWidth = 1764,
    sheetContentHeight = 407
}

SheetInfo.frameIndex =
{

    ["2cbutton-pressed"] = 1,
    ["2cbutton"] = 2,
    ["2dbutton-pressed"] = 3,
    ["2dbutton"] = 4,
    ["2hbutton-pressed"] = 5,
    ["2hbutton"] = 6,
    ["2sbutton-pressed"] = 7,
    ["2sbutton"] = 8,
    ["3cbutton-pressed"] = 9,
    ["3cbutton"] = 10,
    ["3dbutton-pressed"] = 11,
    ["3dbutton"] = 12,
    ["3hbutton-pressed"] = 13,
    ["3hbutton"] = 14,
    ["3sbutton-pressed"] = 15,
    ["3sbutton"] = 16,
    ["4cbutton-pressed"] = 17,
    ["4cbutton"] = 18,
    ["4dbutton-pressed"] = 19,
    ["4dbutton"] = 20,
    ["4hbutton-pressed"] = 21,
    ["4hbutton"] = 22,
    ["4sbutton-pressed"] = 23,
    ["4sbutton"] = 24,
    ["5cbutton-pressed"] = 25,
    ["5cbutton"] = 26,
    ["5dbutton-pressed"] = 27,
    ["5dbutton"] = 28,
    ["5hbutton-pressed"] = 29,
    ["5hbutton"] = 30,
    ["5sbutton-pressed"] = 31,
    ["5sbutton"] = 32,
    ["6cbutton-pressed"] = 33,
    ["6cbutton"] = 34,
    ["6dbutton-pressed"] = 35,
    ["6dbutton"] = 36,
    ["6hbutton-pressed"] = 37,
    ["6hbutton"] = 38,
    ["6sbutton-pressed"] = 39,
    ["6sbutton"] = 40,
    ["7cbutton-pressed"] = 41,
    ["7cbutton"] = 42,
    ["7dbutton-pressed"] = 43,
    ["7dbutton"] = 44,
    ["7hbutton-pressed"] = 45,
    ["7hbutton"] = 46,
    ["7sbutton-pressed"] = 47,
    ["7sbutton"] = 48,
    ["8cbutton-pressed"] = 49,
    ["8cbutton"] = 50,
    ["8dbutton-pressed"] = 51,
    ["8dbutton"] = 52,
    ["8hbutton-pressed"] = 53,
    ["8hbutton"] = 54,
    ["8sbutton-pressed"] = 55,
    ["8sbutton"] = 56,
    ["9cbutton-pressed"] = 57,
    ["9cbutton"] = 58,
    ["9dbutton-pressed"] = 59,
    ["9dbutton"] = 60,
    ["9hbutton-pressed"] = 61,
    ["9hbutton"] = 62,
    ["9sbutton-pressed"] = 63,
    ["9sbutton"] = 64,
    ["acbutton-pressed"] = 65,
    ["acbutton"] = 66,
    ["adbutton-pressed"] = 67,
    ["adbutton"] = 68,
    ["ahbutton-pressed"] = 69,
    ["ahbutton"] = 70,
    ["asbutton-pressed"] = 71,
    ["asbutton"] = 72,
    ["jcbutton-pressed"] = 73,
    ["jcbutton"] = 74,
    ["jdbutton-pressed"] = 75,
    ["jdbutton"] = 76,
    ["jhbutton-pressed"] = 77,
    ["jhbutton"] = 78,
    ["jsbutton-pressed"] = 79,
    ["jsbutton"] = 80,
    ["kcbutton-pressed"] = 81,
    ["kcbutton"] = 82,
    ["kdbutton-pressed"] = 83,
    ["kdbutton"] = 84,
    ["khbutton-pressed"] = 85,
    ["khbutton"] = 86,
    ["ksbutton-pressed"] = 87,
    ["ksbutton"] = 88,
    ["qcbutton-pressed"] = 89,
    ["qcbutton"] = 90,
    ["qdbutton-pressed"] = 91,
    ["qdbutton"] = 92,
    ["qhbutton-pressed"] = 93,
    ["qhbutton"] = 94,
    ["qsbutton-pressed"] = 95,
    ["qsbutton"] = 96,
    ["selectedsquare-yellow"] = 97,
    ["selectedsquare"] = 98,
    ["tcbutton-pressed"] = 99,
    ["tcbutton"] = 100,
    ["tdbutton-pressed"] = 101,
    ["tdbutton"] = 102,
    ["thbutton-pressed"] = 103,
    ["thbutton"] = 104,
    ["tsbutton-pressed"] = 105,
    ["tsbutton"] = 106,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo

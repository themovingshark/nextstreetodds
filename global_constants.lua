
module(..., package.seeall)

--Position global variables 
POSITIONnotset = 0;
POSITIONearly = 1;
POSITIONmiddle = 2;
POSITIONlate =3;
POSITIONsb=4;
POSITIONbb=5;

--indices for moveText, ie moveText[RAISE] = "Raise"
RAISE = 1 
FOLD = 2
RERAISE = 3
CALL = 4
CHECK = 5
moveText = {"Raise", "Fold", "Reraise", "Call","Check"}  --moveText[1] = "Raise"  moveText[2]= "Fold", etc

--Game type
GAMETYPEloose = 0
GAMETYPEtight=1
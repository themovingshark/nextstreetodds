module(..., package.seeall)
possibleStraights = { "a2345", 
                                 "23456",
                                 "34567",
                                 "45678",
                                 "56789",
                                 "6789t",
                                 "789tj",
                                 "89tjq",
                                 "9tjqk",
                                 "tjqka" }
                                 
 possibleOpenEndedStraightDraws = { "a234", 
                                 "2345", 
                                 "3456",
                                 "4567",
                                 "5678",
                                 "6789",
                                 "789t",
                                 "89tj",
                                 "9tjq",
                                 "tjqk",
                                 "jqka" }
                                 
 possibleGutShotStraightDraws = { "a234",  -- need 5 special case for wheel
 								  "a345",  --need 2
                                  "a245",  --need 3
                                  "a235",  --need 4
                                  "2356",  --need 4
                                  "2346",  --need 5
                                  "3467",  --need 5
                                  "4678",  --need 5
                                  "4578" ,  --need 6
                                  "5789",  --need 6
                                  "5689",  --need 7
                                  "689t",  --need 7
                                  "679t" ,   --need 8
                                  "79tj",  --need 8
                                  "78tj",   --need 9
                                  "8tjq",   --need 9
                                  "89jq",   --need 10
                                  "9jqk",    --need 10
                                  "9tqk",   --need jack
                                  "tqka",   --need jack
                                  "tjka",   --need queen
                                  "2456",  --need 3
                                  "3567",  --need 4
                                  "3457",  --need 6
                                  "4568",  --need 7
                                  "5679",  --need 8
                                  "678t",  --need 9
                                  "79tj",  --need 8
                                  "789j",  --need t
                                  "89tq",  --need j
                                  "9tjk",  --need q
                                  "tjqa",  --need k
                                  }
                                 
function CountValues(currentHand)
	print("in function HavePair() with current hand as: "..currentHand)
	local maxNumberOfValue = 0   --max number of any card value
	local cardCount = {}  --will be a table containing number of each type of card, ex {"a"=2,"k"=3,...} is 2 aces, 3 kings, etc
	
	--count aces in hand
	local _, count = string.gsub(currentHand, "a", "")
	if count > maxNumberOfValue then
		maxNumberOfValue = count
	end
	print("number of aces =" .. count)
	cardCount["a"] = count
	
	local _, count = string.gsub(currentHand, "k", "")
	if count > maxNumberOfValue then
		maxNumberOfValue = count
	end
	print("number of kings =" .. count)
	cardCount["k"] = count
	
	local _, count = string.gsub(currentHand, "q", "")
	if count > maxNumberOfValue then
		maxNumberOfValue = count
	end
	print("number of queens =" .. count)
	cardCount["q"] = count
	
	local _, count = string.gsub(currentHand, "j", "")
	if count > maxNumberOfValue then
		maxNumberOfValue = count
	end
	print("number of jacks =" .. count)
	cardCount["j"] = count
	
	local _, count = string.gsub(currentHand, "t", "")
	if count > maxNumberOfValue then
		maxNumberOfValue = count
	end
	print("number of tens =" .. count)
	cardCount["t"] = count
	
	local _, count = string.gsub(currentHand, "9", "")
	if count > maxNumberOfValue then
		maxNumberOfValue = count
	end
	print("number of nines =" .. count)
	cardCount["9"] = count
	
	local _, count = string.gsub(currentHand, "8", "")
	if count > maxNumberOfValue then
		maxNumberOfValue = count
	end
	print("number of eights =" .. count)
	cardCount["8"] = count
	
	local _, count = string.gsub(currentHand, "7", "")
	if count > maxNumberOfValue then
		maxNumberOfValue = count
	end
	print("number of sevens =" .. count)
	cardCount["7"] = count
	
	local _, count = string.gsub(currentHand, "6", "")
	if count > maxNumberOfValue then
		maxNumberOfValue = count
	end
	print("number of sixes =" .. count)
	cardCount["6"] = count
	
	local _, count = string.gsub(currentHand, "5", "")
	if count > maxNumberOfValue then
		maxNumberOfValue = count
	end
	print("number of fives =" .. count)
	cardCount["5"] = count
	
	local _, count = string.gsub(currentHand, "4", "")
	if count > maxNumberOfValue then
		maxNumberOfValue = count
	end
	print("number of fours =" .. count)
	cardCount["4"] = count
	
	local _, count = string.gsub(currentHand, "3", "")
	if count > maxNumberOfValue then
		maxNumberOfValue = count
	end
	print("number of threes =" .. count)
	cardCount["3"] = count
	
	local _, count = string.gsub(currentHand, "2", "")
	if count > maxNumberOfValue then
		maxNumberOfValue = count
	end
	print("number of twos =" .. count)
	cardCount["2"] = count
	
	
	return cardCount
	--[[if maxNumberOfValue == 2 then  --yes we have a pair
		return true
	else
		return false
	end  --]]
end   --end function

function NumPairs(cardCount)
	local nPairs = 0
	for _,v in pairs(cardCount) do
		if v == 2 then
    		nPairs = nPairs +1
		end
	end
	
	return nPairs
end   --end NumPairs

function HaveThreeOfKind(cardCount)
	print("in HaveThreeOfKind")
	for _,v in pairs(cardCount) do
		if v == 3 then
    		return true
		end
	end
	
	return false  -- no 3 of kind
end   --end HaveThreeOfKind

function HaveFourOfKind(cardCount)
	print("in HaveFourOfKind")
	for _,v in pairs(cardCount) do
		if v == 4 then
			print("------------------------------>4 of kind achieved")
    		return true
		end
	end
	
	return false  -- no 3 of kind
end   --end HaveThreeOfKind

function FourFlushCheck(currentHand)
	local _, count1 = string.gsub(currentHand, "h", "")  --checkfor hearts
	local _, count2 = string.gsub(currentHand, "s", "")  --checkfor spades
	local _, count3 = string.gsub(currentHand, "c", "")  --checkfor clubs
	local _, count4 = string.gsub(currentHand, "d", "")  --checkfor diamonds
	if (count1 == 4) or (count2 == 4) or (count3 == 4) or (count4 == 4) then
		print("4 flush is true")
		return true
	else
		print("4 flush is false")
		return false
	end
end  --function fourflushcheck	

function FlushAchieved(currentHand)
	local _, count1 = string.gsub(currentHand, "h", "")  --checkfor hearts
	local _, count2 = string.gsub(currentHand, "s", "")  --checkfor spades
	local _, count3 = string.gsub(currentHand, "c", "")  --checkfor clubs
	local _, count4 = string.gsub(currentHand, "d", "")  --checkfor diamonds
	if (count1 >= 5) or (count2 >= 5) or (count3 >= 5) or (count4 >= 5) then
		print("flush achieved is true")
		return true
	else
		print("flush achieved is false")
		return false
	end
end  --function FlushAchieved	

function OpenEndedStraightDraw(currentHand)
	--[[pickout values from string
		local biggestCount = 0 
		for i=1, #possibleOpenEndedStraightDraws, 1 do
			print("possibleOpenEndedStraightDraws["..i.."]="..possibleOpenEndedStraightDraws[i])
			local _, count1 = string.gsub(possibleOpenEndedStraightDraws[i], params.card1Value, "")
			local _, count2 = string.gsub(possibleOpenEndedStraightDraws[i], params.card2Value, "")
			local _, count3 = string.gsub(possibleOpenEndedStraightDraws[i], params.flopCard1Value, "")
			local _, count4 = string.gsub(possibleOpenEndedStraightDraws[i], params.flopCard2Value, "")
			local _, count5 = string.gsub.possibleOpenEndedStraightDraws[i], params.flopCard3Value, "")
			local totalMatchingCards = count1 + count2 + count3 + count4 + count5
			if totalMatchingCards > biggestCount then
				biggestCount = totalMatchingCards
				print("changing biggestCount to " .. biggestCount)
			end
		end
		if biggestCount == 4 then --open ended straight draw
				--openEndedDraw = true
			return true
			else
			return false
		end
	--]]
end    --end function OpenEndedStraightDraw

function GutShotStraightDraw(currentHand)
end    --end function OpenEndedStraightDraw
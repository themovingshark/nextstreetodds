----------------------------------------------------------------------------------
--
-- scenetemplate.lua
--
----------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()
local ht = require("handtables");
local ot = require("odds")
local pf = require("pokerfuncs")
local widget = require "widget"
--local moveDecider = require ("moveDecider")
local helpText = require("ultimateoddshelp")
local uiGlobals = require("ui_globals")
local constantsGlobals = require ("global_constants")
local params
local currentStartingHand = "";   --starting 2 cards
--Set Global width and height variables
local _W = display.contentWidth;
local _H = display.contentHeight;
local flopCard1Image,  flopCard2Image, flopCard3Image, turnCardImage
--local needleOneValue, needleTwoValue,needleThreeValue,needleFourValue,needleFiveValue,needleSixValue,needleSevenValue,needleEightValue
local needleOneValue, needleTwoValue,needleThreeValue,needleFourValue,needleFiveValue,needleSevenValue,needleEightValue

---------------------------------------------------------------------------------
	--Hide status bar
	display.setStatusBar(display.HiddenStatusBar);
--********************************************************************************
-- function scene:create( event )
--********************************************************************************
function scene:create( event )
	local sceneGroup = self.view
	params = event.params
		
--Set Global width and height variables
	--_W = display.contentWidth;
	--_H = display.contentHeight;

	--local backgroundImage = "images/dashboard-background.png";
	local backgroundImage =uiGlobals.backgroundImage
	local switchSceneSound = audio.loadSound ( uiGlobals.switchSceneSound);
	local needleStartingRotation = -13;
	local needleEndingRotation = 185;
	local maxPercentForDial = 100;
	local ANGLEtype =1;
	local PERCENTtype = 2; 

	local soundOffButton,soundOnButton;
	

		--**********************Locations of display objects
	
	--local dialYposition = 820
	--random hand dial
	--[[local againstRandomDialx = (_W*.5) - 180
	local againstRandomDialy = dialYposition
	local randomHandNeedlex = againstRandomDialx
	local randomHandNeedley = againstRandomDialy
	local randomHandPercentTextx = againstRandomDialx
	local randomHandPercentTexty = againstRandomDialy + 30
	local randomHandDialLabelx = againstRandomDialx
	local randomHandDialLabely = againstRandomDialy +130
	--]]

	local buttonLabelOffset = -45   --text for buttons
	
	--local newHandButtonx = 210
	--local newHandButtony = 110
	-- *********Starting Hand Information setup info
	


    print("ENTERING Mainscreen SCENCE CREATE")
	-- Called when the scene's view does not exist.
	-- 
	-- INSERT code here to initialize the scene
	-- e.g. add display objects to 'sceneGroup', add touch listeners, etc.
	print("in mainscreen params.hand=" .. params.hand);
	print("in mainscreen params.chenformula=" .. params.chenFormula);
	--show background
	local backgroundImg = display.newImageRect( backgroundImage, 640, 1024 )
	backgroundImg.x = display.contentCenterX
	backgroundImg.y = display.contentCenterY

--[[   
	dialOneX = 50
dialOneY = 100
needleOneX = dialOneX
needleOneY= dialOneY
dialOnePercentTextX= dialOneX
dialOnePercentTextY= dialOneY+30
dialOneLabelX=dialOneX
dialOneLabelY=dialOneY+130--]]

   --create text describing what the dials are showing
   local dialDescriptionLabelLine1 = display.newText( uiGlobals.dialDescTextLine1, uiGlobals.dialDescTextLine1X, uiGlobals.dialDescTextLine1Y, native.systemFont, uiGlobals.dialDescLine1FontSize)
   dialDescriptionLabelLine1:setFillColor(unpack(uiGlobals.dialDescTextLine1Color))
   local dialDescriptionLabelLine2 = display.newText( uiGlobals.dialDescTextLine2, uiGlobals.dialDescTextLine2X, uiGlobals.dialDescTextLine2Y, native.systemFont, uiGlobals.dialDescLine2FontSize)
   dialDescriptionLabelLine2:setFillColor(unpack(uiGlobals.dialDescTextLine2Color))
   --hide these at first
   dialDescriptionLabelLine1.isVisible= false
   dialDescriptionLabelLine2.isVisible=false

   --hole, flop and turn card label
   local holeCardLabel = display.newText( uiGlobals.holeCardText, uiGlobals.holeCardTextX, uiGlobals.holeCardTextY, native.systemFont, uiGlobals.holeCardTextFontSize)
   holeCardLabel:setFillColor(unpack(uiGlobals.holeCardTextColor))
   local flopCardLabel = display.newText( uiGlobals.flopCardText, uiGlobals.flopCardTextX, uiGlobals.flopCardTextY, native.systemFont, uiGlobals.flopCardTextFontSize)
   flopCardLabel:setFillColor(unpack(uiGlobals.holeCardTextColor))
   local turnCardLabel = display.newText( uiGlobals.turnCardText, uiGlobals.turnCardTextX, uiGlobals.turnCardTextY, native.systemFont, uiGlobals.turnCardTextFontSize)
   turnCardLabel:setFillColor(unpack(uiGlobals.holeCardTextColor))
   flopCardLabel.isVisible = false
   turnCardLabel.isVisible = false


	--100 is width in pixels before wrapping, 0 height means dont crop letter height
	--Dial One---->1 PAIR
	local dialOne = display.newImageRect( uiGlobals.dialImage, uiGlobals.dialSizeWidth, uiGlobals.dialSizeHeight )
	dialOne.x = uiGlobals.dialOneX
	dialOne.y = uiGlobals.dialOneY
	--local dialOneLabel = display.newText( "dial 1 ", 0, 0,80,0, native.systemFont, positionLabelSize)
	local dialOneLabel = display.newText( uiGlobals.dialOneLabelText, uiGlobals.dialOneLabelX, uiGlobals.dialOneLabelY,native.systemFont, uiGlobals.dialLabelFontSize)
	dialOneLabel.x = uiGlobals.dialOneLabelX
	dialOneLabel.y = uiGlobals.dialOneLabelY
	dialOneLabel:setFillColor(0, 0, 0);
	--local needleOne = display.newImageRect( uiGlobals.needleImage, 700,700 )
	local needleOne = display.newImageRect( uiGlobals.needleImage, uiGlobals.needleSizeWidth, uiGlobals.needleSizeHeight )
	needleOne.x = uiGlobals.needleOneX
	needleOne.y = uiGlobals.needleOneY
	local dialOnePercentText = display.newText( "", 0, 0, native.systemFont, uiGlobals.dialPercentLabelFontSize)
	dialOnePercentText.x = uiGlobals.dialOnePercentTextX
	dialOnePercentText.y = uiGlobals.dialOnePercentTextY
	
	--Dial Two---->STRAIGHT
	local dialTwo = display.newImageRect( uiGlobals.dialImage, uiGlobals.dialSizeWidth, uiGlobals.dialSizeHeight )
	dialTwo.x = uiGlobals.dialTwoX
	dialTwo.y = uiGlobals.dialTwoY
	local dialTwoLabel = display.newText( uiGlobals.dialTwoLabelText, uiGlobals.dialTwoLabelX, uiGlobals.dialTwoLabelY,native.systemFont, uiGlobals.dialLabelFontSize)
	dialTwoLabel.x = uiGlobals.dialTwoLabelX
	dialTwoLabel.y = uiGlobals.dialTwoLabelY
	dialTwoLabel:setFillColor(0, 0, 0);
	local needleTwo = display.newImageRect( uiGlobals.needleImage, uiGlobals.needleSizeWidth, uiGlobals.needleSizeHeight )
	needleTwo.x = uiGlobals.needleTwoX
	needleTwo.y = uiGlobals.needleTwoY
	local dialTwoPercentText = display.newText( "", 0, 0, native.systemFont, uiGlobals.dialPercentLabelFontSize)
	dialTwoPercentText.x = uiGlobals.dialTwoPercentTextX
	dialTwoPercentText.y = uiGlobals.dialTwoPercentTextY

	--Dial Three---->4 of KIND
	local dialThree = display.newImageRect( uiGlobals.dialImage, uiGlobals.dialSizeWidth, uiGlobals.dialSizeHeight )
	dialThree.x = uiGlobals.dialThreeX
	dialThree.y = uiGlobals.dialThreeY
	local dialThreeLabel = display.newText(uiGlobals.dialThreeLabelText, uiGlobals.dialThreeLabelX, uiGlobals.dialThreeLabelY,native.systemFont, uiGlobals.dialLabelFontSize)
	dialThreeLabel.x = uiGlobals.dialThreeLabelX
	dialThreeLabel.y = uiGlobals.dialThreeLabelY
	dialThreeLabel:setFillColor(0, 0, 0);
	local needleThree = display.newImageRect( uiGlobals.needleImage, uiGlobals.needleSizeWidth, uiGlobals.needleSizeHeight )
	needleThree.x = uiGlobals.needleThreeX
	needleThree.y = uiGlobals.needleThreeY
	local dialThreePercentText = display.newText( "", 0, 0, native.systemFont, uiGlobals.dialPercentLabelFontSize)
	dialThreePercentText.x = uiGlobals.dialThreePercentTextX
	dialThreePercentText.y = uiGlobals.dialThreePercentTextY

	
	--Dial Four---->2 PAIR
	local dialFour = display.newImageRect( uiGlobals.dialImage, uiGlobals.dialSizeWidth, uiGlobals.dialSizeHeight )
	dialFour.x = uiGlobals.dialFourX
	dialFour.y = uiGlobals.dialFourY
	local dialFourLabel = display.newText(uiGlobals.dialFourLabelText, uiGlobals.dialFourLabelX, uiGlobals.dialFourLabelY,native.systemFont, uiGlobals.dialLabelFontSize)
	dialFourLabel.x = uiGlobals.dialFourLabelX
	dialFourLabel.y = uiGlobals.dialFourLabelY
	dialFourLabel:setFillColor(0, 0, 0);
	local needleFour = display.newImageRect( uiGlobals.needleImage, uiGlobals.needleSizeWidth, uiGlobals.needleSizeHeight )
	needleFour.x = uiGlobals.needleFourX
	needleFour.y = uiGlobals.needleFourY
	local dialFourPercentText = display.newText( "", 0, 0, native.systemFont, uiGlobals.dialPercentLabelFontSize)
	dialFourPercentText.x = uiGlobals.dialFourPercentTextX
	dialFourPercentText.y = uiGlobals.dialFourPercentTextY
	
	--Dial Five---->FLUSH
	local dialFive = display.newImageRect( uiGlobals.dialImage, uiGlobals.dialSizeWidth, uiGlobals.dialSizeHeight )
	dialFive.x = uiGlobals.dialFiveX
	dialFive.y = uiGlobals.dialFiveY
	local dialFiveLabel = display.newText( uiGlobals.dialFiveLabelText, uiGlobals.dialFiveLabelX, uiGlobals.dialFiveLabelY,native.systemFont, uiGlobals.dialLabelFontSize)
	dialFiveLabel.x = uiGlobals.dialFiveLabelX
	dialFiveLabel.y = uiGlobals.dialFiveLabelY
	dialFiveLabel:setFillColor(0, 0, 0);
	local needleFive = display.newImageRect( uiGlobals.needleImage, uiGlobals.needleSizeWidth, uiGlobals.needleSizeHeight )
	needleFive.x = uiGlobals.needleFiveX
	needleFive.y = uiGlobals.needleFiveY
	local dialFivePercentText = display.newText( "", 0, 0, native.systemFont, uiGlobals.dialPercentLabelFontSize)
	dialFivePercentText.x = uiGlobals.dialFivePercentTextX
	dialFivePercentText.y = uiGlobals.dialFivePercentTextY
	
	
	--Dial Seven----> 3 of KIND
	local dialSeven = display.newImageRect( uiGlobals.dialImage, uiGlobals.dialSizeWidth, uiGlobals.dialSizeHeight )
	dialSeven.x = uiGlobals.dialSevenX
	dialSeven.y = uiGlobals.dialSevenY
	local dialSevenLabel = display.newText( uiGlobals.dialSevenLabelText, uiGlobals.dialSevenLabelX, uiGlobals.dialSevenLabelY,native.systemFont, uiGlobals.dialLabelFontSize)
	dialSevenLabel.x = uiGlobals.dialSevenLabelX
	dialSevenLabel.y = uiGlobals.dialSevenLabelY
	dialSevenLabel:setFillColor(0, 0, 0);
	local needleSeven = display.newImageRect( uiGlobals.needleImage, uiGlobals.needleSizeWidth, uiGlobals.needleSizeHeight )
	needleSeven.x = uiGlobals.needleSevenX
	needleSeven.y = uiGlobals.needleSevenY
	local dialSevenPercentText = display.newText( "", 0, 0, native.systemFont, uiGlobals.dialPercentLabelFontSize)
	dialSevenPercentText.x = uiGlobals.dialSevenPercentTextX
	dialSevenPercentText.y = uiGlobals.dialSevenPercentTextY
	
	--Dial Eight---->FULL HOUSE
	local dialEight = display.newImageRect( uiGlobals.dialImage, uiGlobals.dialSizeWidth, uiGlobals.dialSizeHeight )
	dialEight.x = uiGlobals.dialEightX
	dialEight.y = uiGlobals.dialEightY
	local dialEightLabel = display.newText( uiGlobals.dialEightLabelText, uiGlobals.dialEightLabelX, uiGlobals.dialEightLabelY,native.systemFont, uiGlobals.dialLabelFontSize)
	dialEightLabel.x = uiGlobals.dialEightLabelX
	dialEightLabel.y = uiGlobals.dialEightLabelY
	dialEightLabel:setFillColor(0, 0, 0);
	local needleEight = display.newImageRect( uiGlobals.needleImage, uiGlobals.needleSizeWidth, uiGlobals.needleSizeHeight )
	needleEight.x = uiGlobals.needleEightX
	needleEight.y = uiGlobals.needleEightY
	local dialEightPercentText = display.newText( "", 0, 0, native.systemFont, uiGlobals.dialPercentLabelFontSize)
	dialEightPercentText.x = uiGlobals.dialEightPercentTextX
	dialEightPercentText.y = uiGlobals.dialEightPercentTextY


	--setup rectangle around hole cards
	--local holeBorder = display.newLine( 20, 35, 300, 35,300,200,20,200,20,35 )
	--holeBorder:setStrokeColor( 1, 0, 0, 1 )
	--holeBorder.strokeWidth = 8
	
    --***********setup hole cards
	local card1Image = display.newImageRect( uiGlobals.cardBackImage, uiGlobals.holeCardWidth,uiGlobals.holeCardHeight )
	card1Image.anchorX = 0  --sets anchor point to left side for alignment
	card1Image.x = uiGlobals.card1Imagex
	card1Image.y = uiGlobals.card1Imagey
	local card2Image = display.newImageRect(  uiGlobals.cardBackImage, uiGlobals.holeCardWidth, uiGlobals.holeCardHeight )
	card2Image.anchorX = 0 --sets anchor point to left side for alignment
	card2Image.x = uiGlobals.card2Imagex
	card2Image.y = uiGlobals.card2Imagey
	--add ids to cards
	card1Image.id = "card1"
	card2Image.id = "card2"

	--***********setup flop cards
	flopCard1Image = display.newImageRect( uiGlobals.cardBackImage, uiGlobals.flopCardWidth,uiGlobals.flopCardHeight )
	flopCard1Image.anchorX = 0  --sets anchor point to left side for alignment
	flopCard1Image.x = uiGlobals.flopCard1Imagex
	flopCard1Image.y = uiGlobals.flopCard1Imagey
	
	flopCard2Image = display.newImageRect(  uiGlobals.cardBackImage, uiGlobals.flopCardWidth, uiGlobals.flopCardHeight )
	flopCard2Image.anchorX = 0  --sets anchor point to left side for alignment
	flopCard2Image.x = uiGlobals.flopCard2Imagex
	flopCard2Image.y = uiGlobals.flopCard2Imagey
	
	flopCard3Image = display.newImageRect(  uiGlobals.cardBackImage, uiGlobals.flopCardWidth, uiGlobals.flopCardHeight )
	flopCard3Image.anchorX = 0  --sets anchor point to left side for alignment
	flopCard3Image.x = uiGlobals.flopCard3Imagex
	flopCard3Image.y = uiGlobals.flopCard3Imagey
	--add ids to cards
	flopCard1Image.id = "flopcard1"
	flopCard2Image.id = "flopcard2"
	flopCard3Image.id = "flopcard3"
	
	--hide the flop initially
	flopCard1Image.isVisible = false
	flopCard2Image.isVisible = false
	flopCard3Image.isVisible = false
	
		--***********setup turn card
	turnCardImage = display.newImageRect( uiGlobals.cardBackImage, uiGlobals.turnCardWidth,uiGlobals.turnCardHeight )
	turnCardImage.anchorX = 0  --sets anchor point to left side for alignment
--	print("uiGlobals.turnCard1Imagex    uiGlobals.turnCard1Imagey ".. uiGlobals.turnCard1Imagex .. "    " ..uiGlobals.turnCard1Imagey)
	turnCardImage.x = uiGlobals.turnCardImagex
	turnCardImage.y = uiGlobals.turnCardImagey
	--add id to card
	turnCardImage.id = "turncard"
	turnCardImage.isVisible = false
	
	
--********************************************************************************
-- function onButtonRelease(event)
--********************************************************************************
local function onButtonRelease( event )
	local btn = event.target  --grab the button that was pressed
	--[[if params.soundOn == true then
		local buttonClickSoundChannel = audio.play(buttonClickSound);
	end  --]]
	print("button pressed was " .. btn.id)
	--if (btn.id == "newhand") or (btn.id == "card1") or (btn.id == "card2")then
	if (btn.id == "card1") or (btn.id == "card2") then  --choose hole cards
		params.choosingWhichCards = 1
		params.numcardstochoose = 2  --2 hole cards
		local options = {
		 	effect = uiGlobals.sceneTransitionType,
		  	time = uiGlobals.sceneTransitionTime,
		  	isModal = true,      --dont allow access to parent screen from overlay like acidental button press
		  	params = params
		}
		composer.showOverlay( "cardchooserscreen" ,options)
	elseif (btn.id == "flopcard1") or (btn.id == "flopcard2") or (btn.id == "flopcard3") then  --choose flop cards
		params.choosingWhichCards = 2
		params.numcardstochoose = 3  --3 flop cards
		local options = {
		 	effect = uiGlobals.sceneTransitionType,
		  	time = uiGlobals.sceneTransitionTime,
		  	isModal = true,      --dont allow access to parent screen from overlay like acidental button press
		  	params = params
		}
		composer.showOverlay( "cardchooserscreen" ,options)
	elseif (btn.id == "turncard") then  --choose turn card
		params.choosingWhichCards = 3
		params.numcardstochoose = 1  --1 turn card
		local options = {
		 	effect = uiGlobals.sceneTransitionType,
		  	time = uiGlobals.sceneTransitionTime,
		  	isModal = true,      --dont allow access to parent screen from overlay like acidental button press
		  	params = params
		}
		composer.showOverlay( "cardchooserscreen" ,options)
	--elseif (btn.id == "helphandinfo") then  --show hand info help
		--local alert = native.showAlert( "Starting Hand Information Help", helpText.MainScreenHelpText ,{"OK"})
	elseif (btn.id == "helpmainscreen") then  --main screen help
		print("help text" .. helpText.MainScreenHelpText)
		local alert = native.showAlert( "Help",helpText.MainScreenHelpText,{"OK"})  --helpText.DialsHelpText
	elseif (btn.id == "soundon") then  
		--show sound off button, hide sound on button and turn off sound
		soundOnButton.isVisible = false
		soundOffButton.isVisible = true
		params.soundOn = false;
		_G.isSoundOn = false
		print ("_G.isSoundOn is now false")
	elseif (btn.id == "soundoff") then  
		--show sound on button , hide sound off button and turn on sound	
		params.soundOn = true;
		soundOffButton.isVisible = false
		soundOnButton.isVisible = true
		_G.isSoundOn = true
		print ("_G.isSoundOn is now true")
	elseif (btn.id == "newhand") then
		--local alert = native.showAlert( "New Hand", "Do new hand stuff" ,{"OK"})
		-- Handler that gets notified when the alert closes
		local function onComplete( event )
   		if ( event.action == "clicked" ) then
       		local i = event.index
        	if ( i == 1 ) then
            -- Do nothing; dialog will simply dismiss
        	elseif ( i == 2 ) then
            -- Open URL if "Learn More" (second button) was clicked
            	resetForNewHand()
        	end
   		end --if clicked
	end

-- Show alert with two buttons
local alert = native.showAlert( "New Hand", "Do you want to clear current hand and start a new one?", { "No", "Yes" }, onComplete )

		
	--[[elseif (btn.id =="SoundOn") then
		params.soundOn = false;
		soundOnButton.isVisible = false;
		soundOffButton.isVisible = true;
	elseif (btn.id =="SoundOff") then
		params.soundOn = true;
		soundOffButton.isVisible = false;
	soundOnButton.isVisible = true;   --]]
	end
end
--and button press handler
card1Image:addEventListener("touch", onButtonRelease)
card2Image:addEventListener("touch", onButtonRelease)
flopCard1Image:addEventListener("touch", onButtonRelease)
flopCard2Image:addEventListener("touch", onButtonRelease)
flopCard3Image:addEventListener("touch", onButtonRelease)
turnCardImage:addEventListener("touch", onButtonRelease)

	

local newHandButton = widget.newButton{
    id = "newhand",
    defaultFile = "images/new-hand-button.png",
    overFile = "images/new-hand-button-pressed.png",
    width = uiGlobals.newHandButtonSize, 
    height = uiGlobals.newHandButtonSize,
    onRelease = onButtonRelease
}
newHandButton.x = uiGlobals.newHandButtonx
newHandButton.y = uiGlobals.newHandButtony

--[[local helpMainScreen= widget.newButton{
    	id = "helpmainscreen",
    	defaultFile = "images/help-button.png",
    	overFile = "images/help-button-pressed.png",
    	width = uiGlobals.mainScreenHelpButtonSize+15, 
    	height = uiGlobals.mainScreenHelpButtonSize+15,
    	onRelease = onButtonRelease
	}
	helpMainScreen.x = uiGlobals.mainScreenHelpButtonX
	helpMainScreen.y = uiGlobals.mainScreenHelpButtonY
	--]]

soundOffButton= widget.newButton{
    	id = "soundoff",
    	defaultFile = "images/sound-off.png",
    	overFile = "images/sound-off-pressed.png",
    	width = uiGlobals.mainScreenHelpButtonSize, 
    	height = uiGlobals.mainScreenHelpButtonSize,
    	onRelease = onButtonRelease
	}
	soundOffButton.x = uiGlobals.soundButtonX
	soundOffButton.y = uiGlobals.soundButtonY
	--soundOffButton.isVisible = false
	soundOffButton.isVisible =  not _G.isSoundOn 

soundOnButton= widget.newButton{
    	id = "soundon",
    	defaultFile = "images/sound-on.png",
    	overFile = "images/sound-on-pressed.png",
    	width = uiGlobals.mainScreenHelpButtonSize, 
    	height = uiGlobals.mainScreenHelpButtonSize,
    	onRelease = onButtonRelease
	}
	soundOnButton.x = uiGlobals.soundButtonX
	soundOnButton.y = uiGlobals.soundButtonY
	--soundOnButton.isVisible = true
	soundOnButton.isVisible = _G.isSoundOn 

--[[
local helpHandInfo = widget.newButton{
    id = "helphandinfo",
    defaultFile = "images/help-button.png",
    overFile = "images/help-button-pressed.png",
    width = 65, 
    height = 65,
    onRelease = onButtonRelease
}
helpHandInfo.x = helpHandInfox
helpHandInfo.y = helpHandInfoy
--]]

--helpHandInfo.isVisible = false


--add all display objects to the group (for composer)
--sceneGroup:insert(holeBorder)
sceneGroup:insert(backgroundImg)
sceneGroup:insert(dialOne)
sceneGroup:insert(dialOneLabel)
sceneGroup:insert(needleOne)
sceneGroup:insert(dialOnePercentText)

sceneGroup:insert(dialTwo)
sceneGroup:insert(dialTwoLabel)
sceneGroup:insert(needleTwo)
sceneGroup:insert(dialTwoPercentText)

sceneGroup:insert(dialThree)
sceneGroup:insert(dialThreeLabel)
sceneGroup:insert(needleThree)
sceneGroup:insert(dialThreePercentText)

sceneGroup:insert(dialFour)
sceneGroup:insert(dialFourLabel)
sceneGroup:insert(needleFour)
sceneGroup:insert(dialFourPercentText)

sceneGroup:insert(dialFive)
sceneGroup:insert(dialFiveLabel)
sceneGroup:insert(needleFive)
sceneGroup:insert(dialFivePercentText)

--sceneGroup:insert(dialSix)
--sceneGroup:insert(dialSixLabel)
--sceneGroup:insert(needleSix)
--sceneGroup:insert(dialSixPercentText)

sceneGroup:insert(dialSeven)
sceneGroup:insert(dialSevenLabel)
sceneGroup:insert(needleSeven)
sceneGroup:insert(dialSevenPercentText)

sceneGroup:insert(dialEight)
sceneGroup:insert(dialEightLabel)
sceneGroup:insert(needleEight)
sceneGroup:insert(dialEightPercentText)

sceneGroup:insert(card1Image);
sceneGroup:insert(card2Image);
sceneGroup:insert(flopCard1Image)
sceneGroup:insert(flopCard2Image)
sceneGroup:insert(flopCard3Image)
sceneGroup:insert(turnCardImage)

sceneGroup:insert(dialDescriptionLabelLine1)
sceneGroup:insert(dialDescriptionLabelLine2)

--add buttons
sceneGroup:insert(newHandButton); 
--sceneGroup:insert(helpMainScreen)
sceneGroup:insert(soundOnButton)
sceneGroup:insert(soundOffButton)

sceneGroup:insert(holeCardLabel)
sceneGroup:insert(flopCardLabel)
sceneGroup:insert(turnCardLabel)

--card1Image.id = "card1"
--card2Image.id = "card2"

--********************************************************************************
-- function showNewStartingHandCardGraphics()
--********************************************************************************
local function showNewStartingHandCardGraphics()
	print("In showNewStartingHandCards")
	print("params.card1" .. params.card1)
	print("params.card2" .. params.card2)
	
	local card1Image = display.newImageRect( "images/cards/".. params.card1 ..".png", uiGlobals.holeCardWidth, uiGlobals.holeCardHeight )
	card1Image.anchorX = 0  --sets anchor point to left side for alignment
	card1Image.x = uiGlobals.card1Imagex
	card1Image.y = uiGlobals.card1Imagey
	local card2Image = display.newImageRect( "images/cards/".. params.card2 ..".png", uiGlobals.holeCardWidth, uiGlobals.holeCardHeight )
	card2Image.anchorX = 0  --sets anchor point to left side for alignment
	card2Image.x = uiGlobals.card2Imagex
	card2Image.y = uiGlobals.card2Imagey
	sceneGroup:insert(card1Image)
	sceneGroup:insert(card2Image)
end

--********************************************************************************
-- function showNewFlopCardGraphics()
--********************************************************************************
local function showNewFlopCardGraphics()
	print("In showNewFlopCardGraphics")
	print("params.flopCard1" .. params.flopCard1)
	print("params.flopCard2" .. params.flopCard2)
	print("params.flopCard3" .. params.flopCard3)
	flopCard1Image = display.newImageRect( "images/cards/".. params.flopCard1 ..".png", uiGlobals.flopCardWidth, uiGlobals.flopCardHeight )
	flopCard1Image.anchorX = 0  --sets anchor point to left side for alignment
	flopCard1Image.x = uiGlobals.flopCard1Imagex
	flopCard1Image.y = uiGlobals.flopCard1Imagey
	flopCard2Image = display.newImageRect( "images/cards/".. params.flopCard2 ..".png", uiGlobals.flopCardWidth, uiGlobals.flopCardHeight )
	flopCard2Image.anchorX = 0  --sets anchor point to left side for alignment
	flopCard2Image.x = uiGlobals.flopCard2Imagex
	flopCard2Image.y = uiGlobals.flopCard2Imagey
	flopCard3Image = display.newImageRect( "images/cards/".. params.flopCard3 ..".png", uiGlobals.flopCardWidth, uiGlobals.flopCardHeight )
	flopCard3Image.anchorX = 0  --sets anchor point to left side for alignment
	flopCard3Image.x = uiGlobals.flopCard3Imagex
	flopCard3Image.y = uiGlobals.flopCard3Imagey
	sceneGroup:insert(flopCard1Image)
	sceneGroup:insert(flopCard2Image)
	sceneGroup:insert(flopCard3Image) 
end    --end function showNewFlopCardGraphics

--********************************************************************************
-- function showNewTurnCardGraphics()
--********************************************************************************
local function showNewTurnCardGraphics()
	print("In showNewTurnCardGraphics")
	print("params.turnCard" .. params.turnCard)
	turnCardImage = display.newImageRect( "images/cards/".. params.turnCard ..".png", uiGlobals.turnCardWidth, uiGlobals.turnCardHeight )
	turnCardImage.anchorX = 0  --sets anchor point to left side for alignment
	turnCardImage.x = uiGlobals.turnCardImagex
	turnCardImage.y = uiGlobals.turnCardImagey
	sceneGroup:insert(turnCardImage)
end

--********************************************************************************
-- function setDial(dialNeedle,dialText, value,valueType)
--********************************************************************************
	--[[function setDial, pass in the dial needle object, value, and flag for type of value.  type: PERCENT or ANGLE
	the value field should be a decimal, such as 53 for 53% .  SEND IN PERCENTtype and value 0 to reset --]]
local function setDial (dialNeedle,dialText, value,valueType)
		print("set Dial value: " .. value)
		
		--always reset needles to 0 before rotating
		dialNeedle.rotation = 0;
		--print("setDial:" .. "dialneedle:"..dialNeedle) -- .."  dialText:" .. dialText.id)  --.. "    value:" ..value.."    valuetype:"..valueType)
		dialNeedle.isVisible =true;  --set needle visible by default
		--dialText:setFillColor(255, 26, 170.95, 0, 0.14);  --set dial text to red
		dialText:setFillColor(1, 0.02, 0.05);  --set dial text to red
		if value == 0 then --reset and hide needle and text reguardless of type
			dialNeedle.isVisible = false;
			dialText.text = "--"
		elseif valueType == ANGLEtype then
			dialNeedle:rotate(value);
			--dialText.isVisible = false;
		elseif valueType == PERCENTtype then  --divide percent by 100 before multiplying rotation
			--print("pre rotate:" ..dialNeedle.rotation)
   			dialNeedle:rotate(value/100*needleEndingRotation); 
			--print("post rotate:" ..dialNeedle.rotation)
			
			dialText.text = string.format("%.2f",value)  .. "%"
			--odds:1
			--dialText.text = string.format("%.2f",100/value-1)  .. ":1"
			
			--print("dial value: " .. dialText.text)
			--dialText.text = value  .. "%"
		end
	end  --end function setDial
	

--********************************************************************************
-- function CalculateOdds()
--********************************************************************************
local function CalculateOdds()
	--set needlevalues to 0  in case they arent set during the conditionals
	--needleOneValue, needleTwoValue,needleThreeValue,needleFourValue,needleFiveValue,needleSixValue,needleSevenValue,needleEightValue = 0,0,0,0,0,0,0,0
	needleOneValue, needleTwoValue,needleThreeValue,needleFourValue,needleFiveValue,needleSevenValue,needleEightValue = 0,0,0,0,0,0,0,0

--first see which odds we are working on based on the filled in cards (hole, flop, turn)
	if (#params.selectedHoleCards > 0) and (#params.selectedFlopCards>0) and (#params.selectedTurnCard>0) then   --work on river odds
		print("work on river odds")
		dialDescriptionLabelLine1.isVisible= true
   		dialDescriptionLabelLine2.isVisible=true
		dialDescriptionLabelLine2.text = "Improving On The River"
		local handValues = {}   --all the hand values in one table
		table.insert(handValues,params.card1Value)
		table.insert(handValues,params.card2Value)
		table.insert(handValues,params.flopCard1Value)
		table.insert(handValues,params.flopCard2Value)
		table.insert(handValues,params.flopCard3Value) 
		table.insert(handValues,params.turnCardValue)
		
		local dedupedHandValues = {}  -- hand values with dupes removed
		local hash = {}
		for _,v in ipairs(handValues) do
   			if (not hash[v]) then
     			dedupedHandValues[#dedupedHandValues+1] = v -- you could print here instead of saving to result table if you wanted
       			hash[v] = true
   			end
		end

		print(#handValues .. " hand values")
		for i = 1, #handValues do
			print(handValues[i])
		end
		print(#dedupedHandValues .. " dedupedHandValues hand values")
		for i = 1, #dedupedHandValues do
			print(dedupedHandValues[i])
		end
		--default all needles to 0
		needleOneValue = 0  --1 pair
		needleTwoValue = 0   --straight
		needleThreeValue = 0  --4 of kind
		needleFourValue = 0    --2 pair
		needleFiveValue = 0    --flush
		--needleSixValue = 0
		needleSevenValue = 0  --3 of kind
		needleEightValue = 0  --fullhouse

		--send in current hand as string concat of cards
		local cardCount = pf.CountValues(params.card1..params.card2..params.flopCard1..params.flopCard2..params.flopCard3..params.turnCard)
		local numPairs = pf.NumPairs(cardCount)
		if numPairs == 1 then
			needleOneValue =  100   -- 1 pair
			needleFourValue = 6.5   --2 pair
			needleSevenValue =   4.3   -- 3 of kind
		elseif numPairs > 1 then
			needleFourValue = 100      -- 2 pair
			needleEightValue = 8.7       --full house
		else  -- dont have 1 pair
			needleOneValue = 12.8     --1 pair
		end
		
		if pf.HaveThreeOfKind(cardCount) then
			needleOneValue = 0    --1 pair
			needleSevenValue = 100         --3 of kind
			needleThreeValue =  2.2       --4 of kind
			needleEightValue = 19.6       --full house
		end
		--***CHECK for open ended or gutshot straight draw
		--local openEndedDraw = false
		local biggestCount = 0 
		for i=1, #pf.possibleOpenEndedStraightDraws, 1 do
			print("pf.possibleOpenEndedStraightDraws["..i.."]="..pf.possibleOpenEndedStraightDraws[i])
			local _, count1 = string.gsub(pf.possibleOpenEndedStraightDraws[i], params.card1Value, "")
			local _, count2 = string.gsub(pf.possibleOpenEndedStraightDraws[i], params.card2Value, "")
			local _, count3 = string.gsub(pf.possibleOpenEndedStraightDraws[i], params.flopCard1Value, "")
			local _, count4 = string.gsub(pf.possibleOpenEndedStraightDraws[i], params.flopCard2Value, "")
			local _, count5 = string.gsub(pf.possibleOpenEndedStraightDraws[i], params.flopCard3Value, "")
			local _, count6 = string.gsub(pf.possibleOpenEndedStraightDraws[i], params.turnCardValue, "")
			local totalMatchingCards = count1 + count2 + count3 + count4 + count5 + count6
			if totalMatchingCards > biggestCount then
				biggestCount = totalMatchingCards
				print("changing biggestCount to " .. biggestCount)
			end
		end

		-- adjust biggestCount by removing the number of duplicates subtract out the difference
		-- between the number of values in the handValues table and the dedupedHandValues
		print("Subtract out dedupedHandValues: #handValues and #dedupedHandValues " .. #handValues.. "   ".. #dedupedHandValues)
		biggestCount = biggestCount -(#handValues - #dedupedHandValues)

		if biggestCount == 4 then --open ended straight draw
				--openEndedDraw = true
			needleTwoValue = 17.4
		elseif biggestCount == 5 then  -- straight achieved
			needleTwoValue = 100  --straight
			needleOneValue = 0  --1 pair
			needleFourValue = 0    --2 pair
			needleSevenValue =0  --3 of kind
		end
		--Check for gut shot straight draw
		biggestCount = 0  --reset biggestCount
		for i=1, #pf.possibleGutShotStraightDraws, 1 do
			print("pf.possibleOpenEndedStraightDraws["..i.."]="..pf.possibleGutShotStraightDraws[i])
			local _, count1 = string.gsub(pf.possibleGutShotStraightDraws[i], params.card1Value, "")
			local _, count2 = string.gsub(pf.possibleGutShotStraightDraws[i], params.card2Value, "")
			local _, count3 = string.gsub(pf.possibleGutShotStraightDraws[i], params.flopCard1Value, "")
			local _, count4 = string.gsub(pf.possibleGutShotStraightDraws[i], params.flopCard2Value, "")
			local _, count5 = string.gsub(pf.possibleGutShotStraightDraws[i], params.flopCard3Value, "")
			local _, count6 = string.gsub(pf.possibleGutShotStraightDraws[i], params.turnCardValue, "")
			local totalMatchingCards = count1 + count2 + count3 + count4 + count5 + count6
			if totalMatchingCards > biggestCount then
				biggestCount = totalMatchingCards
				print("changing gut shot biggestCount to " .. biggestCount)
			end
		end

		-- adjust biggestCount by removing the number of duplicates subtract out the difference
		-- between the number of values in the handValues table and the dedupedHandValues
		biggestCount = biggestCount -(#handValues - #dedupedHandValues)

		if biggestCount == 4 then --gut shot straight draw
			needleTwoValue = 8.7  --straight
		end
		
		--Check for straight achieved
		biggestCount = 0 
		for i=1, #pf.possibleStraights, 1 do
			print("pf.possibleStraights["..i.."]="..pf.possibleStraights[i])
			local _, count1 = string.gsub(pf.possibleStraights[i], params.card1Value, "")
			local _, count2 = string.gsub(pf.possibleStraights[i], params.card2Value, "")
			local _, count3 = string.gsub(pf.possibleStraights[i], params.flopCard1Value, "")
			local _, count4 = string.gsub(pf.possibleStraights[i], params.flopCard2Value, "")
			local _, count5 = string.gsub(pf.possibleStraights[i], params.flopCard3Value, "")
			local _, count5 = string.gsub(pf.possibleStraights[i], params.flopCard3Value, "")
			local _, count6 = string.gsub(pf.possibleStraights[i], params.turnCardValue, "")
			local totalMatchingCards = count1 + count2 + count3 + count4 + count5 + count6
			if totalMatchingCards > biggestCount then
				biggestCount = totalMatchingCards
				print("changing straight achieved biggestCount to " .. biggestCount)
				
			end
		end

		-- adjust biggestCount by removing the number of duplicates subtract out the difference
		-- between the number of values in the handValues table and the dedupedHandValues
		biggestCount = biggestCount -(#handValues - #dedupedHandValues)

		if biggestCount >= 5 then --straight achieved
			needleTwoValue = 100
			needleOneValue = 0  --1 pair
			needleFourValue = 0    --2 pair
			needleSevenValue =0  --3 of kind
		end

		if params.fourFlush == true then
			needleFiveValue  =   19.1    --flush
		end 
		if params.flushAchieved == true then
			needleFiveValue = 100  --flush achieved
			--clear all lesser dials
			needleOneValue = 0  --1 pair
			needleTwoValue = 0  --straight
			needleFourValue = 0   --2 pair
			needleSevenValue = 0   --3 of kind
		end


	elseif (#params.selectedHoleCards > 0) and (#params.selectedFlopCards>0) then  --work on turn odds
		print("work on turn odds")
		turnCardLabel.isVisible = true
		dialDescriptionLabelLine1.isVisible= true
   		dialDescriptionLabelLine2.isVisible=true
		dialDescriptionLabelLine2.text = "Improving On The Turn"
		local handValues = {}   --all the hand values in one table
		table.insert(handValues,params.card1Value)
		table.insert(handValues,params.card2Value)
		table.insert(handValues,params.flopCard1Value)
		table.insert(handValues,params.flopCard2Value)
		table.insert(handValues,params.flopCard3Value) 
		
		local dedupedHandValues = {}  -- hand values with dupes removed
		local hash = {}
		for _,v in ipairs(handValues) do
   			if (not hash[v]) then
     			dedupedHandValues[#dedupedHandValues+1] = v -- you could print here instead of saving to result table if you wanted
       			hash[v] = true
   			end
		end

		print(#handValues .. " hand values")
		for i = 1, #handValues do
			print(handValues[i])
		end
		print(#dedupedHandValues .. " dedupedHandValues hand values")
		for i = 1, #dedupedHandValues do
			print(dedupedHandValues[i])
		end

		--default all needles to 0
		needleOneValue = 0
		needleTwoValue = 0 
		needleThreeValue = 0
		needleFourValue = 0
		needleFiveValue = 0
		--needleSixValue = 0
		needleSevenValue = 0 
		needleEightValue = 0
		--send in current hand as string concat of cards
		local cardCount = pf.CountValues(params.card1..params.card2..params.flopCard1..params.flopCard2..params.flopCard3..params.turnCard)
		local numPairs = pf.NumPairs(cardCount)
		print("number of pairs " .. numPairs)
		---[[
		if numPairs == 1 then
			needleOneValue =  100   -- 1 pair
			needleFourValue = 6.5   --2 pair
			needleSevenValue =   4.3   -- 3 of kind
		elseif numPairs > 1 then
			needleFourValue = 100      -- 2 pair
			needleEightValue = 8.7       --full house
		else  -- dont have 1 pair
			needleOneValue = 12.8     --1 pair
		end
		
		if pf.HaveThreeOfKind(cardCount) then
			needleOneValue = 0    --1 pair
			needleFourValue = 0    --2 pair
			needleSevenValue = 100         --3 of kind
			needleThreeValue =  2.1        --4 of kind
			if numPairs ==1 then
				needleOneValue = 0  --1 pair
				needleFourValue = 0    --2 pair
				needleSevenValue=0     --3 of kind
				needleTwoValue = 0  --straight
				needleFiveValue = 0  --flush 
				needleEightValue=100   --full house
			else
				needleEightValue = 15.2        --full house
			end
		end

		if pf.HaveFourOfKind(cardCount) then
			needleOneValue = 0    --1 pair
			needleFourValue = 0    --2 pair
			needleSevenValue = 0         --3 of kind
			needleThreeValue =  100        --4 of kind
			needleEightValue = 0        --full house
		end

		--***CHECK for open ended or gutshot straight draw
		--local openEndedDraw = false
		local biggestCount = 0 
		for i=1, #pf.possibleOpenEndedStraightDraws, 1 do
			print("pf.possibleOpenEndedStraightDraws["..i.."]="..pf.possibleOpenEndedStraightDraws[i])
			local _, count1 = string.gsub(pf.possibleOpenEndedStraightDraws[i], params.card1Value, "")
			local _, count2 = string.gsub(pf.possibleOpenEndedStraightDraws[i], params.card2Value, "")
			local _, count3 = string.gsub(pf.possibleOpenEndedStraightDraws[i], params.flopCard1Value, "")
			local _, count4 = string.gsub(pf.possibleOpenEndedStraightDraws[i], params.flopCard2Value, "")
			local _, count5 = string.gsub(pf.possibleOpenEndedStraightDraws[i], params.flopCard3Value, "")

			local totalMatchingCards = count1 + count2 + count3 + count4 + count5

			if totalMatchingCards > biggestCount then
				biggestCount = totalMatchingCards
				print("changing biggestCount to " .. biggestCount)
			end
		end

		-- adjust biggestCount by removing the number of duplicates subtract out the difference
		-- between the number of values in the handValues table and the dedupedHandValues
		biggestCount = biggestCount -(#handValues - #dedupedHandValues)
		print("adjusting biggestCount in open ended draw to " .. biggestCount)
		if biggestCount == 4 then --open ended straight draw
			needleTwoValue = 17.0
		end
		

		--Check for gut shot straight draw
		biggestCount = 0  --reset biggestCount
		for i=1, #pf.possibleGutShotStraightDraws, 1 do
			print("pf.possibleGutShotStraightDraws["..i.."]="..pf.possibleGutShotStraightDraws[i])
			local _, count1 = string.gsub(pf.possibleGutShotStraightDraws[i], params.card1Value, "")
			local _, count2 = string.gsub(pf.possibleGutShotStraightDraws[i], params.card2Value, "")
			local _, count3 = string.gsub(pf.possibleGutShotStraightDraws[i], params.flopCard1Value, "")
			local _, count4 = string.gsub(pf.possibleGutShotStraightDraws[i], params.flopCard2Value, "")
			local _, count5 = string.gsub(pf.possibleGutShotStraightDraws[i], params.flopCard3Value, "")
			
			local totalMatchingCards = count1 + count2 + count3 + count4 + count5
			print("count1 count2 count3 count4 count5:" .. count1 .. count2 .. count3 .. count4 .. count5)
			if totalMatchingCards > biggestCount then
				biggestCount = totalMatchingCards
				print("changing gut shot biggestCount to " .. biggestCount)
			end
		end

		-- adjust biggestCount by removing the number of duplicates subtract out the difference
		-- between the number of values in the handValues table and the dedupedHandValues
		biggestCount = biggestCount -(#handValues - #dedupedHandValues)
		print("adjusting biggestCount in gutshot draw to " .. biggestCount)

		if biggestCount == 4 then --gut shot straight draw
			needleTwoValue = 8.5  --straight
		end
		
		--Check for straight achieved
		biggestCount = 0 
		print("#pf.possibleStraights=" .. #pf.possibleStraights)
		for i=1, #pf.possibleStraights, 1 do
			print("pf.possibleStraights["..i.."]="..pf.possibleStraights[i])
			local _, count1 = string.gsub(pf.possibleStraights[i], params.card1Value, "")
			local _, count2 = string.gsub(pf.possibleStraights[i], params.card2Value, "")
			local _, count3 = string.gsub(pf.possibleStraights[i], params.flopCard1Value, "")
			local _, count4 = string.gsub(pf.possibleStraights[i], params.flopCard2Value, "")
			local _, count5 = string.gsub(pf.possibleStraights[i], params.flopCard3Value, "")

			print("count1 count2 count3 count4 count5:" .. count1 .. count2 .. count3 .. count4 .. count5)
			local totalMatchingCards = count1 + count2 + count3 + count4 + count5
			if totalMatchingCards > biggestCount then
				biggestCount = totalMatchingCards
				print("changing straight achieved biggestCount to " .. biggestCount)
			end
		end

		-- adjust biggestCount by removing the number of duplicates subtract out the difference
		-- between the number of values in the handValues table and the dedupedHandValues
		biggestCount = biggestCount -(#handValues - #dedupedHandValues)
		print("adjusting biggestCount in straight achieved to " .. biggestCount)

		if biggestCount == 5 then --straight achieved
			needleTwoValue = 100
			needleOneValue = 0  --1 pair
			needleFourValue = 0    --2 pair
			needleSevenValue = 0  --3 of kind
		end

		if params.fourFlush == true then
			needleFiveValue  =   19.1    --flush
		end 
		if params.flushAchieved == true then
			print("------------------------------------>FLUSH ACHIEVED")
			needleFiveValue = 100  --flush achieved
			--clear all lesser dials
			needleOneValue = 0  --1 pair
			needleTwoValue = 0  --straight
			needleFourValue = 0   --2 pair
			needleSevenValue = 0   --3 of kind
		end
			
		--]]
	elseif (#params.selectedHoleCards > 0)  then  --work on flop odds
		print("work on flop odds")
		flopCardLabel.isVisible = true
		dialDescriptionLabelLine1.isVisible= true
   		dialDescriptionLabelLine2.isVisible=true
		dialDescriptionLabelLine2.text = "Improving On The Flop"
		--default all needles to 0
		needleOneValue = 0
		needleTwoValue = 0 
		needleThreeValue = 0
		needleFourValue = 0
		needleFiveValue = 0
		--needleSixValue = 0
		needleSevenValue = 0 
		--************work on pocket pair flop odds
		if params.holePocketPair == true then
			needleOneValue = 100  -- 1 pair
			needleTwoValue = 0 --no straight possible on flop with pocket pairs
			--needleSixValue = 0-- straight flush
			needleFourValue = 16.7   -- 2 pair
			needleFiveValue  = 0 --flush
			needleThreeValue = .24   --4 of kind
			needleSevenValue = 10.76  -- 3 of kind
			needleEightValue = .98  --full house
		
		else --************work on non pocket pair flop odds
			needleOneValue = 32.4 -- 1 pair
			if params.twoToStraight == true then
				needleTwoValue = 1.31        --straight
			else
				needleTwoValue = 0 --no straight possible on flop
			end
			
			--if (params.holeSuited == true) and (params.twoToStraight==true) then
				--needleSixValue = .83 -- straight flush  http://www.cardschat.com/f13/pre-flop-odds-suited-connectors-help-155967/
			--else
				--needleSixValue = 0  --straight flush not possible
			--end
			
			needleFourValue = 4.55  -- 2 pair
			if params.holeSuited == true then
				needleFiveValue=  .84 --flush
			else
				needleFiveValue= 0  --flush
			end
			needleThreeValue = .01   --4 of kind
			needleSevenValue = 1.35  -- 3 of kind
			needleEightValue = .09   --full house
		end
		--[[needleTwoValue =
		needleThreeValue =
		needleFourValue =
		needleFiveValue =
		needleSixValue =
		needleSevenValue =
		needleEightValue =
		--]]
	else  --no cards selected
		print("no cards selected")
		--needleOneValue, needleTwoValue,needleThreeValue,needleFourValue,needleFiveValue,needleSixValue,needleSevenValue,needleEightValue = 0,0,0,0,0,0,0,0
	end  -- end if
end   -- end function CalculateOdds

--[[function SetStartingHandInformation(handGroup,handRank)
	print("in function SetStartingHandInformation " ..handGroup, handRank);
	handGroupText.text =  handgroupLabel .. " " .. handGroup;
	handRankText.text = handrankLabel .. " " ..handRank .. " of 169";
	handRankPercentText.text = handrankPercentLabel .. " " .. string.format("top %.2f",(handRank/169*100)).."%";
	chenFormulaValueText.text = chenFormulaValueLabel .. params.chenFormula
end --]]
--********************************************************************************
-- function UpdateData()
--********************************************************************************
function UpdateData()
	print("********************ENTERING UPDATEDATA*********************")
	print("params.numcardstochoose=" .. params.numcardstochoose)
	print("string.len(currentStartingHand)="..string.len(currentStartingHand) .. "  currentStartingHand="..currentStartingHand) 
	print("UPDATE DATA: params.soundOn=" .. tostring(params.soundOn))
	print("#params.selectedHoleCards=" .. #params.selectedHoleCards)
	--setup current hand as the concat of all cards
	params.hand = params.card1..params.card2..params.flopCard1..params.flopCard2..params.flopCard3..params.turnCard
	--if string.len(currentStartingHand) > 0 then  --only set dials if hand is set
	if (params.numcardstochoose == 2) and (#params.selectedHoleCards == 2) then   --show selected hole cards
		showNewStartingHandCardGraphics();
		--show the blank flop cards
		flopCard1Image.isVisible = true
		flopCard2Image.isVisible = true
		flopCard3Image.isVisible = true
		--SetStartingHandInformation(ht.handRankTable[currentStartingHand].group,ht.handRankTable[currentStartingHand].rank);
		print("calling setDial")
		--setDial(randomHandNeedle, randomHandPercentText,ht.handRankTable[currentStartingHand].againstRandom,PERCENTtype);
		
		print("back from calling setDial")
	elseif (params.numcardstochoose == 3) and (#params.selectedFlopCards == 3) then   --show selected flop cards
		showNewFlopCardGraphics()
		turnCardImage.isVisible = true   --show the blank turn card
	elseif (params.numcardstochoose == 1) and (#params.selectedTurnCard == 1) then -- show selected turn card
		showNewTurnCardGraphics()
	end
	
	--**************************************TO DO
	--Check for straight and flush draws
	 params.fourFlush = pf.FourFlushCheck(params.hand)  --check if we have 4 flush yet
	 params.openEndedStraightDraw = pf.OpenEndedStraightDraw(params.hand)  --check if we have open ended straight
	 params.flushAchieved = pf.FlushAchieved(params.hand)
	--params.gutshotStraightDraw = 
	--params.areConnected = 
	--params.twoToStraight = 
	--params.threeToStraight = 
	--params.fourToStraight =
	--**************************************TO DO
	--Calculate the odds 
	CalculateOdds()
	
	
	-- Update the dials 
	setDial(needleOne,dialOnePercentText,needleOneValue, PERCENTtype)
	setDial(needleTwo,dialTwoPercentText,needleTwoValue, PERCENTtype)
	setDial(needleThree,dialThreePercentText,needleThreeValue, PERCENTtype)
	setDial(needleFour,dialFourPercentText,needleFourValue, PERCENTtype)
    setDial(needleFive,dialFivePercentText,needleFiveValue, PERCENTtype)
	--setDial(needleSix,dialSixPercentText,needleSixValue, PERCENTtype)
	setDial(needleSeven,dialSevenPercentText,needleSevenValue, PERCENTtype)
	setDial(needleEight,dialEightPercentText,needleEightValue, PERCENTtype)
end  --end updatedata

--********************************************************************************
-- function resetAll()
--********************************************************************************
 function resetAll()
		--reset the  dials
		setDial(needleOne, dialOnePercentText,0,PERCENTtype);
		setDial(needleTwo, dialTwoPercentText,0,PERCENTtype);
		setDial(needleThree, dialThreePercentText,0,PERCENTtype);
		setDial(needleFour, dialFourPercentText,0,PERCENTtype);
		setDial(needleFive, dialFivePercentText,0,PERCENTtype);
		--setDial(needleSix, dialSixPercentText,0,PERCENTtype);
		setDial(needleSeven, dialSevenPercentText,0,PERCENTtype);
		setDial(needleEight, dialEightPercentText,0,PERCENTtype);
end   --end function resetAll
	 
print ("initial needleOne.rotate = " .. needleOne.rotation);
resetAll() 


--********************************************************************************
-- function resetForNewHand()
--********************************************************************************
function resetForNewHand()
	--reset all the dials
	--resetAll()

	--first remove the scene, so we can recreate it with the original defaults
	composer.removeScene("mainscreen")

	--*********************
	-- call the same stuff as in the main.lua file to reset all tables
	--***********************
	local options = {
   effect = uiGlobals.startupSceneTransitionType, 
   time = uiGlobals.startupSceneTransitionTime,
   params = {
		hand="",
		card1="",  --hole card 1
		card1Value = "",  --value of card such as 2 or j
		card2="",  --hole card 1
		card2Value = "", 
		chenFormula=0,
		numcardstochoose= 0,
		--soundOn=true,
		soundOn = _G.isSoundOn,
		gameType= 0 , --loose game
		flopCard1 ="",
		flopCard1Value = "",
		flopCard2="",
		flopCard2Value = "",
		flopCard3="",
		flopCard3Value = "",
		turnCard="",
		turnCardValue = "",
		choosingWhichCards = 1,   --1= hole, 2= flop, 3= turn
		completeHand = "",
		selectedHoleCards = {},
		selectedFlopCards = {},
		selectedTurnCard = {},
		alreadySelectedCards = {},
		holePocketPair = false,
		holeSuited = false,
		fourFlush = false,
		flushAchieved = false,
		openEndedStraightDraw = false,
		gutshotStraightDraw = false,
		areConnected = false,
		twoToStraight = false,
		threeToStraight = false,
		fourToStraight = false
	}
}
		 composer.gotoScene( "mainscreen" ,options)

	

end  --end fundtion resetForNewHand


--currentStartingHand = "KTS"; 
currentStartingHand = string.upper(params.hand);   --all table values need upper case for their index

--params.chenFormula = ht.handRank[currentStartingHand].chenFormula;
--print(currentStartingHand);
--set the chen formula if cards have been selected
if string.len(currentStartingHand) > 0 then
	params.chenFormula = ht.handRankTable[currentStartingHand].chenFormula;
	print("chenFormula=" .. ht.handRankTable[currentStartingHand].chenFormula);
end

--TO DO UpdateData()  --update dials and labels

end  --end scene:create

--********************************************************************************
-- function scene:show( event )
--********************************************************************************
function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	--params = event.params
	    print("ENTERING Mainscreen SCENCE SHOW")
	    print("params.hand=" .. params.hand);
	print("params.chenformula=" .. params.chenFormula);
	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		-- 
		-- INSERT code here to make the scene come alive
		--[[ e.g. start timers, begin animation, play audio, etc.
		currentStartingHand = string.upper(params.hand);   --all table values need upper case for their index
		displayRecommendedMove() 

		--set the chen formula if cards have been selected
		if string.len(currentStartingHand) > 0 then
			params.chenFormula = ht.handRankTable[currentStartingHand].chenFormula;
			print("chenFormula=" .. ht.handRankTable[currentStartingHand].chenFormula);
		end
		--]]
		UpdateData()
	end	
end

--*************************************************************************
-- function scene:BackFromChoosingCards()
--*************************************************************************
function scene:BackFromChoosingCards()
	print("Entering ***************************** scene:BackFromChoosingCards")
--[[   
   params = {
		hand="",
		card1="",  --hole card 1
		card2="",  --hole card 1
		chenFormula=0,
		numcardstochoose= 2,
		soundOn=true,
		gameType= 0 , --loose game
		flopCard1 ="",
		flopCard2="",
		flopCard3="",
		turnCard="",
		choosingWhichCards = 1,   --1= hole, 2= flop, 3= turn
		completeHand = "",
		selectedHoleCards = {},
		selectedFlopCards = {},
		selectedTurnCard = {},
		alreadySelectedCards = {}
	}
	--]]
	print("completeHand="..params.completeHand.."  card1="..params.card1.."  card2="..params.card2.."  flopCard1="..params.flopCard1.."  flopCard2="..params.flopCard2.."  flopCard3="..params.flopCard3 .."  turnCard="..params.turnCard)
	--[[print("----selected cards" )
	for pos = 1,#params.alreadySelectedCards do
		print(params.alreadySelectedCards[pos])
  	end
  	print("----selected hole cards" )
  		for pos = 1,#params.selectedHoleCards do
			print(params.selectedHoleCards[pos])
  	end
  	print("----selected hole cards" )
  	  print("----selected flop cards" )
  		for pos = 1,#params.selectedFlopCards do
			print(params.selectedFlopCards[pos])
  	end
  	print("----selected flop cards" )
  	 print("----selected turn card" )
  		for pos = 1,#params.selectedTurnCard do
			print(params.selectedTurnCard[pos])
  	end
	print("----selected turn card" )  --]]
	-- check if showing new hole cards
	--if params.numcardstochoose == 2 then
		resetAll()   --reset dials
		
		UpdateData()  --update dials and labels
		--show flop cards if cards where selected
		--[[if string.len(params.completeHand) > 0 then
			flopCard1Image.isVisible = true
			flopCard2Image.isVisible = true
			flopCard3Image.isVisible = true
		end  --]]
	--end  --params.numcardstochoose == 2
	
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end	
end


function scene:destroy( event )
	local sceneGroup = self.view
	print("in Mainscreen scene:destroy")
	-- Called prior to the removal of scene's "view" (sceneGroup)
	-- 
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.
	--earlyPosImage:removeSelf()
--earlyPosImage = nil
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
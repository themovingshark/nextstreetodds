<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>3</int>
        <key>texturePackerVersion</key>
        <string>3.3.4</string>
        <key>fileName</key>
        <string>/Users/brad/Google Drive/Apps/Corona Projects/Ultimate Poker Odds Dashboard/_Project source/textpackerlayout.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>corona-imagesheet</string>
        <key>textureFileName</key>
        <filename>images/cardbuttons/cardbuttonsheet.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename></filename>
            </struct>
            <key>lua</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>images/cardbuttons/cardbuttonsheet.lua</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>cleanTransparentPixels</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>innerPadding</key>
            <uint>0</uint>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>heuristicMask</key>
            <false/>
            <key>pivotPoint</key>
            <enum type="SpriteSettings::PivotPoint">Center</enum>
        </struct>
        <key>fileList</key>
        <array>
            <filename>images/cardbuttons/2cbutton-pressed.png</filename>
            <filename>images/cardbuttons/2cbutton.png</filename>
            <filename>images/cardbuttons/2dbutton-pressed.png</filename>
            <filename>images/cardbuttons/2dbutton.png</filename>
            <filename>images/cardbuttons/2hbutton-pressed.png</filename>
            <filename>images/cardbuttons/2hbutton.png</filename>
            <filename>images/cardbuttons/2sbutton-pressed.png</filename>
            <filename>images/cardbuttons/2sbutton.png</filename>
            <filename>images/cardbuttons/3cbutton-pressed.png</filename>
            <filename>images/cardbuttons/3cbutton.png</filename>
            <filename>images/cardbuttons/3dbutton-pressed.png</filename>
            <filename>images/cardbuttons/3dbutton.png</filename>
            <filename>images/cardbuttons/3hbutton-pressed.png</filename>
            <filename>images/cardbuttons/3hbutton.png</filename>
            <filename>images/cardbuttons/3sbutton-pressed.png</filename>
            <filename>images/cardbuttons/3sbutton.png</filename>
            <filename>images/cardbuttons/4cbutton-pressed.png</filename>
            <filename>images/cardbuttons/4cbutton.png</filename>
            <filename>images/cardbuttons/4dbutton-pressed.png</filename>
            <filename>images/cardbuttons/4dbutton.png</filename>
            <filename>images/cardbuttons/4hbutton-pressed.png</filename>
            <filename>images/cardbuttons/4hbutton.png</filename>
            <filename>images/cardbuttons/4sbutton-pressed.png</filename>
            <filename>images/cardbuttons/4sbutton.png</filename>
            <filename>images/cardbuttons/5cbutton-pressed.png</filename>
            <filename>images/cardbuttons/5cbutton.png</filename>
            <filename>images/cardbuttons/5dbutton-pressed.png</filename>
            <filename>images/cardbuttons/5dbutton.png</filename>
            <filename>images/cardbuttons/5hbutton-pressed.png</filename>
            <filename>images/cardbuttons/5hbutton.png</filename>
            <filename>images/cardbuttons/5sbutton-pressed.png</filename>
            <filename>images/cardbuttons/5sbutton.png</filename>
            <filename>images/cardbuttons/6cbutton-pressed.png</filename>
            <filename>images/cardbuttons/6cbutton.png</filename>
            <filename>images/cardbuttons/6dbutton-pressed.png</filename>
            <filename>images/cardbuttons/6dbutton.png</filename>
            <filename>images/cardbuttons/6hbutton-pressed.png</filename>
            <filename>images/cardbuttons/6hbutton.png</filename>
            <filename>images/cardbuttons/6sbutton-pressed.png</filename>
            <filename>images/cardbuttons/6sbutton.png</filename>
            <filename>images/cardbuttons/7cbutton-pressed.png</filename>
            <filename>images/cardbuttons/7cbutton.png</filename>
            <filename>images/cardbuttons/7dbutton-pressed.png</filename>
            <filename>images/cardbuttons/7dbutton.png</filename>
            <filename>images/cardbuttons/7hbutton-pressed.png</filename>
            <filename>images/cardbuttons/7hbutton.png</filename>
            <filename>images/cardbuttons/7sbutton-pressed.png</filename>
            <filename>images/cardbuttons/7sbutton.png</filename>
            <filename>images/cardbuttons/8cbutton-pressed.png</filename>
            <filename>images/cardbuttons/8cbutton.png</filename>
            <filename>images/cardbuttons/8dbutton-pressed.png</filename>
            <filename>images/cardbuttons/8dbutton.png</filename>
            <filename>images/cardbuttons/8hbutton-pressed.png</filename>
            <filename>images/cardbuttons/8hbutton.png</filename>
            <filename>images/cardbuttons/8sbutton-pressed.png</filename>
            <filename>images/cardbuttons/8sbutton.png</filename>
            <filename>images/cardbuttons/9cbutton-pressed.png</filename>
            <filename>images/cardbuttons/9cbutton.png</filename>
            <filename>images/cardbuttons/9dbutton-pressed.png</filename>
            <filename>images/cardbuttons/9dbutton.png</filename>
            <filename>images/cardbuttons/9hbutton-pressed.png</filename>
            <filename>images/cardbuttons/9hbutton.png</filename>
            <filename>images/cardbuttons/9sbutton-pressed.png</filename>
            <filename>images/cardbuttons/9sbutton.png</filename>
            <filename>images/cardbuttons/acbutton-pressed.png</filename>
            <filename>images/cardbuttons/acbutton.png</filename>
            <filename>images/cardbuttons/adbutton-pressed.png</filename>
            <filename>images/cardbuttons/adbutton.png</filename>
            <filename>images/cardbuttons/ahbutton-pressed.png</filename>
            <filename>images/cardbuttons/ahbutton.png</filename>
            <filename>images/cardbuttons/asbutton-pressed.png</filename>
            <filename>images/cardbuttons/asbutton.png</filename>
            <filename>images/cardbuttons/jcbutton-pressed.png</filename>
            <filename>images/cardbuttons/jcbutton.png</filename>
            <filename>images/cardbuttons/jdbutton-pressed.png</filename>
            <filename>images/cardbuttons/jdbutton.png</filename>
            <filename>images/cardbuttons/jhbutton-pressed.png</filename>
            <filename>images/cardbuttons/jhbutton.png</filename>
            <filename>images/cardbuttons/jsbutton-pressed.png</filename>
            <filename>images/cardbuttons/jsbutton.png</filename>
            <filename>images/cardbuttons/kcbutton-pressed.png</filename>
            <filename>images/cardbuttons/kcbutton.png</filename>
            <filename>images/cardbuttons/kdbutton-pressed.png</filename>
            <filename>images/cardbuttons/kdbutton.png</filename>
            <filename>images/cardbuttons/khbutton-pressed.png</filename>
            <filename>images/cardbuttons/khbutton.png</filename>
            <filename>images/cardbuttons/ksbutton-pressed.png</filename>
            <filename>images/cardbuttons/ksbutton.png</filename>
            <filename>images/cardbuttons/qcbutton-pressed.png</filename>
            <filename>images/cardbuttons/qcbutton.png</filename>
            <filename>images/cardbuttons/qdbutton-pressed.png</filename>
            <filename>images/cardbuttons/qdbutton.png</filename>
            <filename>images/cardbuttons/qhbutton-pressed.png</filename>
            <filename>images/cardbuttons/qhbutton.png</filename>
            <filename>images/cardbuttons/qsbutton-pressed.png</filename>
            <filename>images/cardbuttons/qsbutton.png</filename>
            <filename>images/cardbuttons/selectedsquare-yellow.png</filename>
            <filename>images/cardbuttons/selectedsquare.png</filename>
            <filename>images/cardbuttons/tcbutton-pressed.png</filename>
            <filename>images/cardbuttons/tcbutton.png</filename>
            <filename>images/cardbuttons/tdbutton-pressed.png</filename>
            <filename>images/cardbuttons/tdbutton.png</filename>
            <filename>images/cardbuttons/thbutton-pressed.png</filename>
            <filename>images/cardbuttons/thbutton.png</filename>
            <filename>images/cardbuttons/tsbutton-pressed.png</filename>
            <filename>images/cardbuttons/tsbutton.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
    </struct>
</data>
